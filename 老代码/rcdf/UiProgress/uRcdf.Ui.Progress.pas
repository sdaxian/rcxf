unit uRcdf.Ui.Progress;

interface

uses
  System.SysUtils, System.Classes, System.Generics.Collections,
  uRcdf.Arc, uRcdf.Ui.ProgressHud;

type
  // ===========================================================================

  /// <summary> UI线程执行队列 </summary>
  IUiQueue = interface
    // 注意目前 Delphi 10.3 的新语法 var x := y；闭包捕获 x 时，引用
    // 计数会错误处理，因此 对于 x 需要 var x: T begin 方式定义
    procedure Queue(AProc: TProc);
  end;

  // ===========================================================================

  /// <summary> 后台线程执行体 </summary>
  /// <remarks>
  ///   <para>
  ///     和 TProgressQueue 一起实现后台线程任务执行能力。executing
  ///     首先在后台线程被调用执行，完毕后，completion 在 UI线程被调
  ///     用。
  ///   </para><para>
  ///     也可以使用两个匿名方法代替
  ///   </para>
  /// </remarks>
  /// <author> 孙毅 </author>
  IProgress = interface
    /// <summary> 后台执行 </summary>
    /// <param name="aUiHandler">用于UI方面处理</param>
    procedure Executing(const aUiQueue: IUiQueue);
    /// <summary> UI 完成 </summary>
    procedure Completion();
  end;

  // ===========================================================================

  /// <summary> 带 提示视图 后台线程执行体 </summary>
  /// <remarks>
  ///   TProgressUtils 用来进行带 提示视图 的后台执行。
  /// </remarks>
  /// <author> 孙毅 </author>
  IHudProgress = interface
    /// <summary> 对话框初始化，对话框会有默认设置，在这里可以设置进
    ///    一步设置 </summary>
    /// <param name="aHud">进度 HUD </param>
    procedure InitHud(const aHud: IProgressHud);
    /// <summary> 后台执行</summary>
    /// <param name="aHud">进度 HUD ，如要修改，必须在 UI 线程</param>
    /// <param name="aUiHandler">用于UI方面处理</param>
    procedure Executing(const aHud: IProgressHud; const aUiQueue: IUiQueue);
    /// <summary> UI 完成 </summary>
    /// <remarks>
    ///   不需要 aHud.Hide，会自动调用
    /// </remarks>
    /// <param name="aHud">进度 HUD </param>
    /// <param name="aCanceled">真：被取消了(不支持，目前固定为 False)</param>
    procedure Completion(const aHud: IProgressHud; aCanceled: Boolean);
  end;

  // ===========================================================================

  /// <summary> 可取消后台执行 </summary>
  /// <remarks>
  ///   实现这个接口，当取消发生时，cancel() 会被调用
  /// </remarks>
  /// <author> 孙毅 </author>
  IProgressCancelable = interface
    /// <summary> 取消，在这里进行具体的取消操作 </summary>
    procedure Cancel();
  end;

  // ===========================================================================

  THudProgressBase = class;
  IHudProgressBase = interface(IArcObject)
    ['{E934B61C-2BE6-4BB1-B568-D273B16892A8}']
    function O: THudProgressBase;
  end;

  /// <summary> 同时实现 IHudProgress 和 IProgressCancelable 的类，作
  ///   基类使用  </summary>
  THudProgressBase = class(TArcObject, IHudProgressBase, IHudProgress,
    IProgressCancelable)
  private
    function O: THudProgressBase;
  protected
    procedure InitHud(const aHud: IProgressHud); virtual;
    procedure Executing(const aHud: IProgressHud;
      const aUiQueue: IUiQueue); virtual;
    procedure Completion(const aHud: IProgressHud; aCanceled: Boolean); virtual;
    procedure Cancel(); virtual;
  end;

  // ===========================================================================

  TUiQueueProc = reference to procedure (const aUiQueue: IUiQueue);

  TProgressQueue = class;
  IProgressQueue = interface(IArcObject)
    ['{9AB2CAE7-5916-4CB5-A48C-A6BC5D00797A}']
    function O: TProgressQueue;
  end;

  /// <summary> 后台执行队列 </summary>
  /// <remarks>
  ///   <para>
  ///     和 IProgress 一起实现后台线程任务执行能力。IRrogress 的
  ///     executing 后台执行体，在后台线程被串行顺次执行。
  ///   </para><para>
  ///     使用 createQueue 创建的实例，可以顺次执行多个 IRrogress，但
  ///     是如果不再需要执行 IRrogress，则必须调用 releaseQueue 释放，
  ///     否则后台线程不会结束。
  ///   </para><para>
  ///     不建议直接使用进行后台执行，推荐通过 TProgressUtils 进行。
  ///   </para><para>
  ///     TODO:
  ///     不会出现循环引用，因为：
  ///       - ProgressQueue 引用 FThread
  ///       - FThread 当有 IProgress 时，引用 ProgressQueue 和 IProgress
  ///       - 再 IProgress 执行完毕后 FThread 释放引用的 ProgressQueue。
  ///   </para><para>
  ///     由上分析，不存在循环引用的情况，且会释放该释放的引用，因此不
  ///     需要像在 android.java 里一样增加手工引用方式。
  ///   </para>
  /// </remarks>
  /// <author> 孙毅 </author>
  TProgressQueue = class(TArcObject, IProgressQueue)
  // ---- 构造函数 -------------------------------------------------------------
  private
    constructor CreateInner(aOwner: TObject = nil);
  public
    constructor Create();
    destructor Destroy; override;
  // ---- 私有 -----------------------------------------------------------------
  private
    function O: TProgressQueue;
  private
    // 线程
    FThread: TThread;

  // ---- 公有 方法 ------------------------------------------------------------
  public
    /// <summary> 创建，用此方法创建的，当使用完毕后，必须销毁，以后考虑用池的方式 </summary>
    class function CreateQueue(): IProgressQueue;
    /// <summary> 销毁 </summary>
    class procedure ReleaseQueue(var aQueue: IProgressQueue);

    /// <summary> 后台执行，使用 IProgress </summary>
    procedure ExecProgress(const aProgress: IProgress); overload;
    /// <summary> 后台执行，使用 匿名函数 </summary>
    procedure ExecProgress(const aExecuting: TUiQueueProc;
      const aCompletion: TProc); overload;
  end;

implementation

type

  TListItem = class;
  IListItem = interface(IArcObject)
    ['{AABE4241-9876-484A-B574-4AFF2E825A7B}']
    function O: TListItem;
  end;
  TListItem = class(TArcObject, IListItem)
  private
    function O: TListItem;
  public
    Owner: IInterface;
    // 真： Progress 假：Executing Completion
    Mode: Boolean;
    // 或者是这个
    Progress: IProgress;
    // 或者是这两个
    Executing: TUiQueueProc;
    Completion: TProc;
  end;

  TProgressThread = class(TThread)
  private
    FList: TList<IListItem>;
    FUiQueue: IUiQueue;

    // 出栈，或超时，超时返回 nil
    function PopOrWait(aTimeout: Integer): IListItem;
    procedure DoProgress;
  protected
    procedure Execute; override;
    procedure TerminatedSet; override;
  public
    constructor Create();
    destructor Destroy; override;

    procedure MyQueue(AProc: TProc);

    // 强引用 aOwner，避免自动销毁
    procedure Push(const aOwner: IInterface; const aProgress: IProgress); overload;
    // 强引用 aOwner，避免自动销毁
    procedure Push(const aOwner: IInterface; const aExecuting: TUiQueueProc;
      const aCompletion: TProc); overload;
  end;

  TUiQueue = class(TInterfacedObject, IUiQueue)
  private
    FOwner: TProgressThread;
    procedure Queue(AProc: TProc);
  public
    constructor Create(aOwner: TProgressThread);
    procedure ClearOwner();
  end;

{ TListItem }

function TListItem.O: TListItem;
begin
  Result := Self;
end;

{ TProgressThread }

constructor TProgressThread.Create;
begin
  inherited Create(True);
  FList := TList<IListItem>.Create;
  FUiQueue := TUiQueue.Create(Self);

  FreeOnTerminate := True;
end;

destructor TProgressThread.Destroy;
begin
  inherited;
  FreeAndNil(FList);

  (FUiQueue as TUiQueue).ClearOwner;
  FUiQueue := nil;
end;

procedure TProgressThread.DoProgress;
var
  mListItem: IListItem;
begin
  mListItem := PopOrWait(3000);
  if mListItem = nil then
    Exit;

  try
    try
      if mListItem.O.Mode then
      begin
        if Assigned(mListItem.O.Progress) then
          mListItem.O.Progress.Executing(FUiQueue)
      end
      else
      begin
        if Assigned(mListItem.O.Executing) then
          mListItem.O.Executing(FUiQueue)
      end;
    except
      // 吞掉异常
    end;
  finally
    Queue(procedure ()
      begin
        // 这会是在 主线程执行，UI 端的异常不吞掉
        if mListItem.O.Mode then
        begin
          if Assigned(mListItem.O.Progress) then
            mListItem.O.Progress.Completion
        end
        else
        begin
          if Assigned(mListItem.O.Completion) then
            mListItem.O.Completion()
        end;
      end);
  end;
end;

procedure TProgressThread.Execute;
begin
  while not Terminated do
    DoProgress;
end;

procedure TProgressThread.MyQueue(AProc: TProc);
begin
  ForceQueue(Self, procedure ()
  begin
    AProc();
  end);
end;

function TProgressThread.PopOrWait(aTimeout: Integer): IListItem;
begin
  MonitorEnter(FList);
  try
    if (not Terminated) and (FList.Count <= 0) then
      MonitorWait(FList, aTimeout);

    if Terminated or (FList.Count <= 0) then
      Exit(nil);

    Result := FList[0];
    FList.Delete(0);
  finally
    MonitorExit(FList);
  end;
end;

procedure TProgressThread.Push(const aOwner: IInterface;
  const aExecuting: TUiQueueProc; const aCompletion: TProc);
var
  mListItem: IListItem;
begin
  MonitorEnter(FList);
  try
    mListItem := TListItem.Create;
    mListItem.O.Owner := aOwner;
    mListItem.O.Mode := False;
    mListItem.O.Executing := aExecuting;
    mListItem.O.Completion := aCompletion;
    FList.Add(mListItem);

    MonitorPulse(FList);
  finally
    MonitorExit(FList);
  end;
end;

procedure TProgressThread.TerminatedSet;
begin
  MonitorEnter(FList);
  try
    MonitorPulse(FList);
  finally
    MonitorExit(FList);
  end;
end;

procedure TProgressThread.Push(const aOwner: IInterface; const aProgress: IProgress);
var
  mListItem: IListItem;
begin
  MonitorEnter(FList);
  try
    mListItem := TListItem.Create;
    mListItem.O.Owner := aOwner;
    mListItem.O.Mode := True;
    mListItem.O.Progress := aProgress;
    FList.Add(mListItem);

    MonitorPulse(FList);
  finally
    MonitorExit(FList);
  end;
end;

{ TUiQueue }

procedure TUiQueue.ClearOwner;
begin
  FOwner := nil;
end;

constructor TUiQueue.Create(aOwner: TProgressThread);
begin
  FOwner := aOwner;
end;

procedure TUiQueue.Queue(AProc: TProc);
begin
  if Assigned(FOwner) and Assigned(AProc) then
    FOwner.MyQueue(AProc);
end;

{ THudProgressBase }

procedure THudProgressBase.Cancel;
begin
  // 默认啥也不做
end;

procedure THudProgressBase.Completion(const aHud: IProgressHud;
  aCanceled: Boolean);
begin
  // 默认啥也不做
end;

procedure THudProgressBase.Executing(const aHud: IProgressHud;
  const aUiQueue: IUiQueue);
begin
  // 默认啥也不做
end;

procedure THudProgressBase.InitHud(const aHud: IProgressHud);
begin
  // 默认啥也不做
end;

function THudProgressBase.O: THudProgressBase;
begin
  Result := Self;
end;

{ TProgressQueue }

constructor TProgressQueue.Create;
begin
  raise Exception.Create('使用 CreateQueue');
end;

constructor TProgressQueue.CreateInner(aOwner: TObject);
begin
  FThread := TProgressThread.Create;
  FThread.Start;
end;

class function TProgressQueue.CreateQueue: IProgressQueue;
begin
  Result := TProgressQueue.CreateInner();
end;

destructor TProgressQueue.Destroy;
begin
  FThread.Terminate;
  FThread := nil;
  inherited;
end;

procedure TProgressQueue.ExecProgress(const aExecuting: TUiQueueProc;
  const aCompletion: TProc);
var
  mOwner: IInterface;
begin
  // 必须这么转一下，否则 有可能出现错误的引用计数减少
  mOwner := Self;
  TProgressThread(FThread).Push(mOwner, aExecuting, aCompletion);
end;

procedure TProgressQueue.ExecProgress(const aProgress: IProgress);
var
  mOwner: IInterface;
begin
  // 必须这么转一下，否则 有可能出现错误的引用计数减少
  mOwner := Self;
  TProgressThread(FThread).Push(mOwner, aProgress);
end;

function TProgressQueue.O: TProgressQueue;
begin
  Result := Self;
end;

class procedure TProgressQueue.ReleaseQueue(var aQueue: IProgressQueue);
begin
  aQueue := nil;
end;


end.
