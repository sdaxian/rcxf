unit uUi2.Utils.TCustomProgressHud;

interface

uses
  System.Classes,System.UITypes,System.Types,System.SysUtils,
  FMX.Controls,FMX.Types,FMX.Objects,FMX.Layouts,
  FMX.StdCtrls,FMX.Graphics,FMX.Filter.Effects,
  FMX.Forms,FMX.Effects,


  uRcdf.Arc,
  uRcdf.Ui.ProgressHud;
type
    TCustomHudControls = class;
    ICustomHudControls = interface(IArcObject)
      ['{C8287A99-A459-462D-8096-333023FC57AF}']
      function O: TCustomHudControls;
    end;
    /// <summary> Hud 控件组 </summary>
    /// <author> 孙毅 </author>
    TCustomHudControls = class(TArcObject, ICustomHudControls)
    private
      function O: TCustomHudControls;
    private
      FControls: array of TControl;
      // 根控件
      Root: TControl;
      // 背景控件
      Bround: TControl;
      // 内容容器
      Content: TControl;
      // 文本控件
      Text: TText;
      FFillRGB: TFillRGBEffect;
      FTextColor: TAlphaColor;
      FAniIndicatorColor: TAlphaColor;
      FShadowColor: TAlphaColor;
      FTextFamily: TFontName;
      FTextSize: Single;
    public
      property TextColor: TAlphaColor write FTextColor;
      property TextSize: Single write FTextSize;
      property TextFamily: TFontName write FTextFamily;
      /// <summary>旋转控件颜色</summary>
      property AniIndicatorColor: TAlphaColor write FAniIndicatorColor;
      /// <summary>旋转控件投影颜色</summary>
      property ShadowColor: TAlphaColor write FShadowColor;
    public
      procedure Build(aOwner: TComponent);
      procedure Reset;
      procedure SetText(const aText: string);
      procedure SetBackgroundOpacity(aOpacity: Single);
    end;


  TCustomControlProgressHud = class;
  ICustomControlProgressHud = interface(IArcObject)
    ['{3B5D02BD-7868-4B6D-8F15-E3649C302560}']
    function O: TCustomControlProgressHud;
  end;

   /// <summary> 对话框形式的进度型 HUD </summary>
  TCustomControlProgressHud = class(TArcObject, ICustomControlProgressHud, IProgressHud)
  // ---- 类型定义 -------------------------------------------------------------
  private type
  // ---- 实现接口 IProgressHud -----------------------------------------------------
  private
    procedure Show(const aProc: TProc);
    procedure Hide;
    procedure SetText(const aText: string);

  // ---- 私有 -----------------------------------------------------------------
  private
    function O: TCustomControlProgressHud;
  private
    FParentControl: TFmxObject;
    FHudControls: ICustomHudControls;
    FText: string;
    FBackgroundOpacity: Single;
    procedure SetAniIndicatorColor(const Value: TAlphaColor);
    procedure SetTextColor(const Value: TAlphaColor);
    procedure SetTextFamily(const Value: TFontName);
    procedure SetTextSize(const Value: Single);
  public
      property TextColor: TAlphaColor write SetTextColor;
      property TextSize: Single write SetTextSize;
      property TextFamily: TFontName write SetTextFamily;
      /// <summary>旋转控件颜色</summary>
      property AniIndicatorColor: TAlphaColor write SetAniIndicatorColor;

  public
    /// <param name="aParentControl"> 参照用的父容器控件 </param>
    constructor Create(aParentControl: TFmxObject;
      aBackgroundOpacity: Single = 0.2);
    destructor Destroy; override;
  end;


implementation

{ THudControls }

procedure TCustomHudControls.Build(aOwner: TComponent);
var
  mLayout: TLayout;
  mBround: TRectangle;
  mContent: TLayout;
  mAniIndicator: TAniIndicator;
  mText: TText;
  mFillRGB: TFillRGBEffect;
  mShadow: TShadowEffect;
begin
  mLayout := TLayout.Create(aOwner);
  mLayout.Align := TAlignLayout.Contents;
  mLayout.HitTest := True;

  mBround := TRectangle.Create(aOwner);
  mBround.Align := TAlignLayout.Contents;
  mBround.Fill.Color := TAlphaColorRec.Black;
  mBround.Stroke.Kind := TBrushKind.None;
  mBround.Opacity := 1;
  mBround.Parent := mLayout;
  mBround.HitTest := False;

  mContent := TLayout.Create(aOwner);
  mContent.Align := TAlignLayout.Center;
  mContent.Size.Size := TSizeF.Create(137, 50);
  mContent.Parent := mLayout;
  mContent.HitTest := False;

  mAniIndicator := TAniIndicator.Create(aOwner);
  mAniIndicator.Align := TAlignLayout.Left;
  mAniIndicator.Enabled := True;
  mAniIndicator.Parent := mContent;
  mAniIndicator.HitTest := False;

  mFillRGB := TFillRGBEffect.Create(nil);
  mFillRGB.Parent := mAniIndicator;
  mFillRGB.Color := FAniIndicatorColor;

  mShadow := TShadowEffect.Create(nil);
  mShadow.Parent := mAniIndicator;
  mShadow.ShadowColor := FAniIndicatorColor;

  mText := TText.Create(aOwner);
  mText.Align := TAlignLayout.Client;
  mText.Margins.Left := 15;
  mText.TextSettings.HorzAlign := TTextAlign.Leading;
  mText.Parent := mContent;
  mText.HitTest := False;

  SetLength(FControls, 5);
  FControls[0] := mLayout;
  FControls[1] := mBround;
  FControls[2] := mContent;
  FControls[3] := mAniIndicator;
  FControls[4] := mText;


  Root := mLayout;
  Bround := mBround;
  Content := mContent;
  Text := mText;
  FFillRGB := mFillRGB;
  Text.Color := FTextColor;
  Text.Font.Family := FTextFamily;
  Text.Font.Size := FTextSize;
end;

function TCustomHudControls.O: TCustomHudControls;
begin
  Result := Self;
end;

procedure TCustomHudControls.Reset;
var
  i: Integer;
begin
  try
    if Root <> nil then
      Root.Parent := nil;
  except
  end;

  Root := nil;
  Text := nil;

  for i := High(FControls) downto 0 do
  begin
    try
      FControls[i].Parent := nil;
    except
    end;
  end;

  for i := High(FControls) downto 0 do
  begin
    try
      FControls[i].Free;
      FControls[i] := nil;
    except
      FControls[i] := nil;
    end;
  end;
  SetLength(FControls, 0);
  if Assigned(FFillRGB)  then
    FFillRGB := nil ;
end;

procedure TCustomHudControls.SetBackgroundOpacity(aOpacity: Single);
begin
   if (Root = nil) or (Bround = nil) then
    Exit;

  if Bround.Opacity <> aOpacity then
    Bround.Opacity := aOpacity;

end;

procedure TCustomHudControls.SetText(const aText: string);
begin
  if (Root = nil) or (Text = nil) or (Content = nil) then
    Exit;

  if aText = '' then
  begin
    Text.Visible := False;
    Content.Size.Width := 50;
    Text.Text := '';
  end
  else
  begin
    Text.Visible := True;
    Content.Size.Width := 300;
    Text.Text := aText;
  end;

end;

{ TCustomProgressHud }

constructor TCustomControlProgressHud.Create(aParentControl: TFmxObject;
  aBackgroundOpacity: Single);
begin
  FParentControl := aParentControl;
  FHudControls := TCustomHudControls.Create;
  FBackgroundOpacity := aBackgroundOpacity;
end;

destructor TCustomControlProgressHud.Destroy;
begin
  if (FHudControls.O.Root <> nil) then
    raise Exception.Create('ProgressHud 没有结束');
  inherited;
end;

procedure TCustomControlProgressHud.Hide;
begin
  if (FHudControls.O.Root = nil) then
    Exit;

  FHudControls.O.Reset;
end;

function TCustomControlProgressHud.O: TCustomControlProgressHud;
begin
  Result := Self;
end;

procedure TCustomControlProgressHud.SetAniIndicatorColor(const Value: TAlphaColor);
begin
  FHudControls.O.AniIndicatorColor := Value;
end;

procedure TCustomControlProgressHud.SetText(const aText: string);
begin
  FText := aText;
  FHudControls.O.SetText(aText);
end;

procedure TCustomControlProgressHud.SetTextColor(const Value: TAlphaColor);
begin
  FHudControls.O.TextColor := Value;
end;

procedure TCustomControlProgressHud.SetTextFamily(const Value: TFontName);
begin
  FHudControls.O.TextFamily := Value;
end;

procedure TCustomControlProgressHud.SetTextSize(const Value: Single);
begin
  FHudControls.O.TextSize := Value;
end;

procedure TCustomControlProgressHud.Show(const aProc: TProc);
begin
  if (FHudControls.O.Root = nil) then
    FHudControls.O.Build(FParentControl);

  FHudControls.O.SetBackgroundOpacity(FBackgroundOpacity);
  FHudControls.O.SetText(FText);

  FHudControls.O.Root.Parent := FParentControl;

  if Assigned(aProc) then
    aProc();
end;

end.
