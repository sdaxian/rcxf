unit uRcdf.Ui.ProgressUtils;

interface

uses
  System.SysUtils, FMX.Types, FMX.Forms, FMX.Controls, FMX.Layouts,
  uRcdf.Ui.Progress, uRcdf.Ui.ProgressHud;

type

  TProgressUtilsHudInitHudProc = reference to procedure (
    const aHud: IProgressHud);
  TProgressUtilsHudExecutingProc = reference to procedure (
    const aHud: IProgressHud; const aUiQueue: IUiQueue);
  TProgressUtilsHudCompletionProc = reference to procedure (
    const aHud: IProgressHud; aCanceled: Boolean);

  /// <summary> 后台执行工具集 </summary>
  /// <remarks>
  ///   提供一些进行后台操作的类函数。在 Delphi 里使用匿名函数更为方便。
  /// </remarks>
  /// <author>孙毅</author>
  TProgressUtils = class abstract
  public
    constructor Create();

  // ---- 公有 方法 ------------------------------------------------------------
  public
    /// <summary> 后台执行 使用 IProgress </summary>
    /// <param name="aQueue">队列, 或 nil 则自动生成一个队列</param>
    /// <param name="aProgress">执行体</param>
    class procedure Progress(const aQueue: IProgressQueue;
      const aProgress: IProgress); overload;

    /// <summary> 后台执行，使用匿名函数 </summary>
    /// <param name="aQueue">队列, 或 nil 则自动生成一个队列</param>
    /// <param name="aExecuting">后台执行闭包</param>
    /// <param name="aCompletion">UI 完成闭包</param>
    class procedure Progress(const aQueue: IProgressQueue;
      const aExecuting: TUiQueueProc; const aCompletion: TProc); overload;

    // ------------------------------------------------------------------

    /// <summary> 带 HUD 的后台执行 </summary>
    /// <remarks>
    ///   <para>
    ///     如果 aProgress 也实现 IProgressCancelanle 接口，则 cancel
    ///      会被调用。（目前不支持）。
    ///   </para><para>
    ///     内部实现通过调用 progress 函数执行。
    ///   </para>
    /// </remarks>
    /// <param name="aHud">进度型 HUD 界面（IProgressHud），
    //    或 IRoot 且 TCustomForm 内部 使用 TDialogProgressHud，
    //    或 nil 内部 使用 TDialogProgressHud，其它异常
    /// </param>
    /// <param name="aMessage">HUD 信息文字</param>
    /// <param name="aCancelable">是否可以取消（返回键、触摸空白区域）
    ///    （目前不支持，设为假）</param>
    /// <param name="aQueue">队列, 或 nil 则自动生成一个队列</param>
    /// <param name="aProgress">执行体</param>
    class procedure ProgressHud(const aHud: IInterface;
      const aMessage: string; aCancelable: Boolean;
      const aQueue: IProgressQueue; const aProgress: IHudProgress); overload;

    /// <summary> 带 HUD 的后台执行 </summary>
    /// <remarks>
    ///   <para>
    ///     如果 aProgress 也实现 IProgressCancelanle 接口，则 cancel
    ///      会被调用。（目前不支持）。
    ///   </para><para>
    ///     内部实现通过调用 progress 函数执行。
    ///   </para>
    /// </remarks>
    /// <param name="aHud">进度型 HUD 界面（IProgressHud），
    //    或 IRoot 且 TCustomForm 内部 使用 TDialogProgressHud，
    //    或 nil 内部 使用 TDialogProgressHud，其它异常
    /// </param>
    /// <param name="aMessage">HUD 信息文字</param>
    /// <param name="aCancelable">是否可以取消（返回键、触摸空白区域）
    ///    （目前不支持，设为假）</param>
    /// <param name="aQueue">队列, 或 nil 则自动生成一个队列</param>
    /// <param name="aInitHudProc">初始化闭包，可选值，参见 IHudProgress</param>
    /// <param name="aExecutingProc">后台执行闭包，参见 IHudProgress</param>
    /// <param name="aCompletionProc">UI 完成闭包，参见 IHudProgress</param>
    class procedure ProgressHud(const aHud: IInterface;
      const aMessage: string; aCancelable: Boolean;
      const aQueue: IProgressQueue;
      const aInitHudProc: TProgressUtilsHudInitHudProc;
      const aExecutingProc: TProgressUtilsHudExecutingProc;
      const aCompletionProc: TProgressUtilsHudCompletionProc); overload;
  end;

  // ---------------------------------------------------------------------------

  /// <summary> UI 完好标记 </summary>
  /// <remarks>
  ///   Delphi UI 有些元素，比如 TForm 等，当被销毁后，有时是无法
  ///   准确知道其是否还有效，是否完好。因此可以利用这个接口，利用
  ///   设置销毁状态来判断是非完好。一般多用于后台线程。
  /// </remarks>
  /// <author>孙毅</author>
  IUiGoodFlag = interface
    procedure Destroyed();
    function IsDestroyed: Boolean;
  end;

  TUiGoodFlag = class(TInterfacedObject, IUiGoodFlag)
  private
    FDestroyed: Boolean;
  public
    procedure Destroyed();
    function IsDestroyed: Boolean;
  end;


implementation


{ TProgressUtils }

constructor TProgressUtils.Create;
begin
  raise Exception.Create('方法类，不允许创建');
end;

class procedure TProgressUtils.Progress(const aQueue: IProgressQueue;
  const aExecuting: TUiQueueProc; const aCompletion: TProc);
var
  mQueue: IProgressQueue;
  mNeedRelease: Boolean;
begin
  mNeedRelease := False;
  try
    if aQueue = nil then
    begin
      mQueue := TProgressQueue.CreateQueue();
      mNeedRelease := True;
    end
    else
      mQueue := aQueue;

    mQueue.O.ExecProgress(aExecuting, aCompletion);
  finally
    if mNeedRelease and (mQueue <> nil) then
      TProgressQueue.ReleaseQueue(mQueue);
  end;
end;

class procedure TProgressUtils.Progress(const aQueue: IProgressQueue;
  const aProgress: IProgress);
var
  mQueue: IProgressQueue;
  mNeedRelease: Boolean;
begin
  mNeedRelease := False;
  try
    if aQueue = nil then
    begin
      mQueue := TProgressQueue.CreateQueue();
      mNeedRelease := True;
    end
    else
      mQueue := aQueue;

    mQueue.O.ExecProgress(aProgress);
  finally
    if mNeedRelease and (mQueue <> nil) then
      TProgressQueue.ReleaseQueue(mQueue);
  end;
end;

class procedure TProgressUtils.ProgressHud(const aHud: IInterface;
  const aMessage: string; aCancelable: Boolean; const aQueue: IProgressQueue;
  const aProgress: IHudProgress);
begin
  ProgressHud(aHud, aMessage, aCancelable, aQueue,
    procedure (const aHud: IProgressHud)
    begin
      if aProgress <> nil then
        aProgress.InitHud(aHud);
    end,
    procedure (const aHud: IProgressHud; const aUiQueue: IUiQueue)
    begin
      if aProgress <> nil then
        aProgress.Executing(aHud, aUiQueue);
    end,
    procedure (const aHud: IProgressHud; aCanceled: Boolean)
    begin
      if aProgress <> nil then
        aProgress.Completion(aHud, aCancelable);
    end);
end;

class procedure TProgressUtils.ProgressHud(const aHud: IInterface;
  const aMessage: string; aCancelable: Boolean; const aQueue: IProgressQueue;
  const aInitHudProc: TProgressUtilsHudInitHudProc;
  const aExecutingProc: TProgressUtilsHudExecutingProc;
  const aCompletionProc: TProgressUtilsHudCompletionProc);
var
  mHud: IProgressHud;
  mRoot: IRoot;
  mControl: IControl;
  mControlObj: TFmxObject;
begin
  mHud := nil;
  if aHud = nil then
    mHud := TDialogProgressHud.Create(nil)
  else if Supports(aHud, IProgressHud, mHud) then
    begin end
  else if Supports(aHud, IRoot, mRoot) then
  begin
    if mRoot.GetObject is TCustomForm then
      mHud := TDialogProgressHud.Create(TCustomForm(mRoot.GetObject));
  end
  else if Supports(aHud, IControl, mControl) then
  begin
    mControlObj := mControl.GetObject;
    // 如果是 TCustomScrollBox，TCustomScrollBox 很可能会清空内部控件而引起
    // 混乱。
    if mControlObj is TCustomScrollBox then
      raise Exception.Create('HUD 不能是 TCustomScrollBox 的子类');

    if mControl.GetObject is TControl then
      mHud := TControlProgressHud.Create(TControl(mControl.GetObject));
  end;
  if mHud = nil then
    raise Exception.Create('没有 HUD');

  // 目前不支持取消，以后考虑以某种方式提供(比如点击空白区域等)

  if Assigned(aInitHudProc) then
    aInitHudProc(mHud);

  mHud.SetText(aMessage);
  mHud.Show(procedure ()
  begin
    Progress(aQueue,
      procedure (const aUiQueue: IUiQueue)
      begin
        if Assigned(aExecutingProc) then
          aExecutingProc(mHud, aUiQueue);
      end,
      procedure ()
      begin
        try
          if Assigned(aCompletionProc) then
            aCompletionProc(mHud, False);
        finally
          mHud.Hide;
        end;
      end);
  end);
end;

{ TUiGoodFlag }

procedure TUiGoodFlag.Destroyed;
begin
  FDestroyed := True;
end;

function TUiGoodFlag.IsDestroyed: Boolean;
begin
  Result := FDestroyed;
end;

end.
