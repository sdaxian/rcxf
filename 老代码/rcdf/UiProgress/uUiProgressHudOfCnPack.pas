unit uUiProgressHudOfCnPack;

interface

uses
  System.SysUtils, System.Classes, System.Types, FMX.Forms, // FMX.Types,

  uRcdf.Arc,
  uRcdf.Ui.ProgressHud,
  uUiProgress;

type
  TProgressHudOfCnPack = class;
  IProgressHudOfCnPack = interface(IProgressHud)
    ['{5229FF53-A82E-4833-9560-223297D58119}']
    function O: TProgressHudOfCnPack;
  end;

   /// <summary> 使用 CnPack 的 HUD </summary>
  TProgressHudOfCnPack = class(TArcObject, IProgressHudOfCnPack, IProgressHud)
  // ---- 类型定义 -------------------------------------------------------------
  private type
  // ---- 实现接口 IProgressHud -----------------------------------------------------
  private
    procedure Show(const aProc: TProc);
    procedure Hide;
    procedure SetText(const aText: string);

  // ---- 私有 -----------------------------------------------------------------
  private
    function O: TProgressHudOfCnPack;
  private
    FText: string;
  public
    procedure UpdateProcess(aCurr: Integer; aMax: Integer);
    procedure UpdateSubTitle(const aTitle: string);
    procedure HideSubTitle();
    procedure CycleProcess();
  public
    constructor Create();
    destructor Destroy; override;
  end;


implementation

{ TProgressHudOfCnPack }

constructor TProgressHudOfCnPack.Create();
begin
end;

procedure TProgressHudOfCnPack.CycleProcess;
begin
  uUiProgress.CycleProcess();
end;

destructor TProgressHudOfCnPack.Destroy;
begin
  inherited;
end;

procedure TProgressHudOfCnPack.Hide;
begin
  uUiProgress.HideProgress();
end;

procedure TProgressHudOfCnPack.HideSubTitle;
begin
  uUiProgress.HideProgressSubTitle();
end;

function TProgressHudOfCnPack.O: TProgressHudOfCnPack;
begin
  Result := Self;
end;

procedure TProgressHudOfCnPack.SetText(const aText: string);
begin
  FText := aText;
  uUiProgress.UpdateProgressTitle(FText);
end;

procedure TProgressHudOfCnPack.Show(const aProc: TProc);
begin
  uUiProgress.ShowProgress(FText, 100);

  if Assigned(aProc) then
    aProc();
end;

procedure TProgressHudOfCnPack.UpdateProcess(aCurr, aMax: Integer);
begin
  uUiProgress.UpdateProgressMax(aMax);
  uUiProgress.UpdateProgress(aCurr);
end;

procedure TProgressHudOfCnPack.UpdateSubTitle(const aTitle: string);
begin
  uUiProgress.UpdateProgressSubTitle(aTitle);
end;

end.
