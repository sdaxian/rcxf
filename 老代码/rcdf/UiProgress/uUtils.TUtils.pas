unit uUtils.TUtils;

interface

uses
  FMX.Types,System.SysUtils,
  uRcdf.Ui.ProgressHud,uRcdf.Ui.ProgressUtils,uRcdf.Ui.Progress,
  uUi2.Utils.TCustomProgressHud;


type
  TUiUtils = class
    /// <summary>多线程处理方法</summary>
    class procedure ProgressHud(aControl:TFmxObject;
     aText:string;aInitProc,aCompletionProc:TProc;
     aIsLightColor:Boolean = False);
  end;

implementation

{ TUtils }

class procedure TUiUtils.ProgressHud(aControl: TFmxObject; aText: string;
  aInitProc, aCompletionProc: TProc; aIsLightColor: Boolean);
var
  mHud: IProgressHud;
begin
  case aIsLightColor of
    True:
      begin
        mHud := TCustomControlProgressHud.Create(aControl);
        (mHud as TCustomControlProgressHud).TextColor := $FFFFF1EA;
        (mHud as TCustomControlProgressHud).AniIndicatorColor := $FFFFF1EA;
        (mHud as TCustomControlProgressHud).TextFamily := '微软雅黑';
        (mHud as TCustomControlProgressHud).TextSize := 14;
      end;
    False: mHud := TControlProgressHud.Create(aControl);
  end;

  TProgressUtils.ProgressHud(mHud, aText, False, nil, nil,
    procedure (const aHud: IProgressHud; const aUiQueue: IUiQueue)
    begin
      if Assigned(aInitProc) then
        aInitProc;
    end,
    procedure (const aHud: IProgressHud; aCanceled: Boolean)
    begin
      if Assigned(aCompletionProc) then
        aCompletionProc;
    end);
end;

end.
