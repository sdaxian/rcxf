unit uUiProgress;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Effects,
  FMX.Objects, FMX.Layouts, FMX.StdCtrls, FMX.Controls.Presentation,
  FMX.Helpers.Win, FMX.Platform.Win, Winapi.Windows;

type
  TProgressForm = class(TForm)
    Rectangle1: TRectangle;
    GlowEffect1: TGlowEffect;
    lblTitle: TLabel;
    Label2: TLabel;
    ProgressBar1: TProgressBar;
    tmr1: TTimer;
    lbl1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure tmr1Timer(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    FPerLabel: TLabel;
    procedure ShowSubTitle();
    procedure HideSubTitle();
  public
    { Public declarations }
  end;

procedure ShowProgress(const Title: string; AMax: Integer = 100);
{* 显示进度条窗体，参数为窗体标题以及最大值，默认 100（百分比形式），可自定义成其他值}
procedure HideProgress;
{* 关闭进度条窗体}
procedure UpdateProgress(Value: Integer);
{* 更新当前进度，参数为进度值：当 Max 为 100 时可接受范围为 0..100，此时 Value 代表百分比}
procedure UpdateProgressTitle(const Title: string);
{* 更新进度条窗体标题，参数为标题}
procedure UpdateProgressMax(Value: Integer);
{* 更新进度条最大值，参数为新的最大值}
procedure ProgressBringToFront;

procedure UpdateProgressSubTitle(const SubTitle: string);
procedure HideProgressSubTitle();
procedure CycleProcess;

implementation

{$R *.fmx}

var
  ProgressForm: TProgressForm = nil;  // 进度条窗体实例
  FormList: Pointer = nil;   // 被禁用的窗体列表指针
  DisableTaskWindowsCount: Integer = 0;

//procedure TProgressForm.Create(AOwner: TComponent);
//begin
//  inherited;
//  FPerLabel := TLabel.Create(Self);
//  FPerLabel.Text := '    '; // 100%
//  FPerLabel.Parent := Label2.Parent;
//  FPerLabel.Position.Y := Label2.Position.Y;
//  FPerLabel.Position.X := ProgressBar1.Position.X + ProgressBar1.Width - FPerLabel.Width;
//end;

procedure TProgressForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
//
end;

procedure TProgressForm.FormCreate(Sender: TObject);
begin
  Height := 81;
  Width := 255;

  //FormStyle := TFormStyle.StayOnTop;

  FPerLabel := TLabel.Create(Self);
  FPerLabel.Text := '    '; // 100%
  FPerLabel.Parent := Label2.Parent;
  FPerLabel.Position.Y := Label2.Position.Y;
  FPerLabel.Position.X := ProgressBar1.Position.X + ProgressBar1.Width - FPerLabel.Width;
  ProgressBar1.Tag := 0;
end;

procedure ShowProgress(const Title: string; AMax: Integer = 100);
begin
  if not Assigned(ProgressForm) then
  begin
    ProgressForm := TProgressForm.Create(Application);
  end
  else
    ProgressForm.BringToFront;
  ProgressForm.lblTitle.Text := Title;
  ProgressForm.lbl1.Text := '';
  ProgressForm.ProgressBar1.Max := AMax;
  ProgressForm.ProgressBar1.Value := 0.0;
  ProgressForm.tmr1.Enabled := True;
  ProgressForm.Show;

  if DisableTaskWindowsCount = 0 then
    FormList := DisableTaskWindows(FormToHWND(ProgressForm));
  Inc(DisableTaskWindowsCount);

  ProgressForm.Invalidate;
end;

procedure ProgressBringToFront;
begin
  ProgressForm.BringToFront;
end;

// 关闭窗体
procedure HideProgress;
begin
  if not Assigned(ProgressForm) then Exit;

  Dec(DisableTaskWindowsCount);
  if DisableTaskWindowsCount <= 0 then
  begin
    EnableTaskWindows(FormList);
    DisableTaskWindowsCount := 0;
  end;

  ProgressForm.tmr1.Enabled := False;
  ProgressForm.Hide;
  //Application.ProcessMessages;
  //ProgressForm.Free;
  //ProgressForm := nil;
end;

// 更新进度
procedure UpdateProgress(Value: Integer);
begin
  if Assigned(ProgressForm) then
  begin
    ProgressForm.ProgressBar1.Tag := 0;
    ProgressForm.ProgressBar1.Value := Value;
    if ProgressForm.ProgressBar1.Max > 0 then
      ProgressForm.FPerLabel.Text := Format('%-3d%%', [Round(Value/ProgressForm.ProgressBar1.Max * 100)])
    else
      ProgressForm.FPerLabel.Text := Format('%-3d%%', [0]);

    ProgressForm.Invalidate;
    // Application.ProcessMessages;
  end;
end;

// 更新标题
procedure UpdateProgressTitle(const Title: string);
begin
  if Assigned(ProgressForm) then
  begin
    ProgressForm.lblTitle.Text := Title;
    ProgressForm.Invalidate;
    // Application.ProcessMessages;
  end;
end;

procedure UpdateProgressSubTitle(const SubTitle: string);
begin
  if Assigned(ProgressForm) then
  begin
    ProgressForm.ShowSubTitle();
    ProgressForm.lbl1.Text := SubTitle;
    ProgressForm.Invalidate;
    // Application.ProcessMessages;
  end;
end;

procedure HideProgressSubTitle();
begin
  if Assigned(ProgressForm) then
  begin
    ProgressForm.HideSubTitle();
    ProgressForm.Invalidate;
    // Application.ProcessMessages;
  end;
end;

procedure CycleProcess;
begin
  ProgressForm.ProgressBar1.Tag := 1;
  ProgressForm.ProgressBar1.Value := 0.0;
  ProgressForm.ProgressBar1.Max := 100;
  ProgressForm.FPerLabel.Text := '';
end;

// 更新进度条最大值
procedure UpdateProgressMax(Value: Integer);
begin
  if Assigned(ProgressForm) then
  begin
    ProgressForm.ProgressBar1.Tag := 0;
    ProgressForm.ProgressBar1.Max := Value;
    ProgressForm.Invalidate;;
    //Application.ProcessMessages;
  end;
end;

procedure TProgressForm.HideSubTitle;
begin
  if not lbl1.Visible then
    Exit;

  Height := 81;
  lbl1.Visible := False;
  Label2.Position.Y := lbl1.Position.Y;

  FPerLabel.Position.Y := Label2.Position.Y;
  FPerLabel.Position.X := ProgressBar1.Position.X + ProgressBar1.Width - FPerLabel.Width;
end;

procedure TProgressForm.ShowSubTitle;
begin
  if lbl1.Visible then
    Exit;

  Height := 105;
  lbl1.Visible := True;
  lbl1.Position.Y := Label2.Position.Y;

  Label2.Position.Y := lbl1.Position.Y + lbl1.Size.Height + 5;

  FPerLabel.Position.Y := Label2.Position.Y;
  FPerLabel.Position.X := ProgressBar1.Position.X + ProgressBar1.Width - FPerLabel.Width;
end;

procedure TProgressForm.tmr1Timer(Sender: TObject);
var
  mNewValue: Single;
begin
  BringToFront;
  if ProgressBar1.Tag = 1 then
  begin
    mNewValue := ProgressBar1.Value + 5;
    if mNewValue > ProgressBar1.Max then
      mNewValue := 0;
    ProgressBar1.Value := mNewValue;
  end;

end;

end.
