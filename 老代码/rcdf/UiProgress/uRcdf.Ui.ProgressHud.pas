unit uRcdf.Ui.ProgressHud;

interface

uses
  System.SysUtils, System.Classes, System.UITypes, System.Types, FMX.Forms,
  FMX.Controls, FMX.Dialogs, FMX.Layouts, FMX.Types, FMX.Objects, FMX.Graphics,
  FMX.StdCtrls,
  uRcdf.Arc;

type
  // ===========================================================================

  /// <summary> 进度型 HUD 界面接口（可以实现为如对话框，控件等） </summary>
  /// <author> 孙毅 </author>
  IProgressHud = interface
    ['{D5DE03F8-9C98-4844-B6C7-64A75935454F}']
    /// <summary> 显示创建界面 </summary>
    /// <param name="aProc"> 显示后被调用，因为 ShowModal 不结束函数不
    ///  会返回，所以提供这个参数 </param>
    procedure Show(const aProc: TProc);
    /// <summary> 隐藏销毁界面 </summary>
    procedure Hide;
    /// <summary> 设置文字 </summary>
    procedure SetText(const aText: string);
  end;

  // ===========================================================================

  THudControls = class;
  IHudControls = interface(IArcObject)
    ['{C1C73410-FE5C-4204-8078-93F3A293EF61}']
    function O: THudControls;
  end;

  /// <summary> Hud 控件组 </summary>
  /// <author> 孙毅 </author>
  THudControls = class(TArcObject, IHudControls)
  private
    function O: THudControls;
  private
    FControls: array of TControl;
  public
    // 根控件
    Root: TControl;
    // 背景控件
    Bround: TControl;
    // 内容容器
    Content: TControl;
    // 文本控件
    Text: TText;
  public
    procedure Build(aOwner: TComponent);
    procedure Reset;
    procedure SetText(const aText: string);
    procedure SetBackgroundOpacity(aOpacity: Single);
  end;

  // ===========================================================================

  TDialogProgressHud = class;
  IDialogProgressHud = interface(IArcObject)
    ['{28C64A21-BCF7-4601-8CC8-FBA9A6BF970F}']
    function O: TDialogProgressHud;
  end;

  /// <summary> 对话框形式的进度型 HUD </summary>
  TDialogProgressHud = class(TArcObject, IDialogProgressHud, IProgressHud)
  // ---- 类型定义 -------------------------------------------------------------
  private type
  // ---- 实现接口 IProgressHud -----------------------------------------------------
  private
    procedure Show(const aProc: TProc);
    procedure Hide;
    procedure SetText(const aText: string);

  // ---- 私有 -----------------------------------------------------------------
  private
    function O: TDialogProgressHud;
  private
    FReferenceForm: TCustomForm;
    FForm: TForm;
    FHudControls: IHudControls;
    FText: string;
    FBackgroundOpacity: Single;
    FShowProc: TProc;

    procedure FormOnShow(Sender: TObject);
    procedure FormOnClose(Sender: TObject; var Action: TCloseAction);
    procedure RootOnMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
  public
    /// <param name="aReferenceForm"> 参照用的窗体，用来确定位置和大
    ///  小 </param>
    constructor Create(aReferenceForm: TCustomForm = nil;
      aBackgroundOpacity: Single = 0.2);
    destructor Destroy; override;
  end;

  TControlProgressHud = class;
  IControlProgressHud = interface(IArcObject)
    ['{63D69148-91BB-4522-95CD-804E8BF8C5A7}']
    function O: TControlProgressHud;
  end;

   /// <summary> 对话框形式的进度型 HUD </summary>
  TControlProgressHud = class(TArcObject, IControlProgressHud, IProgressHud)
  // ---- 类型定义 -------------------------------------------------------------
  private type
  // ---- 实现接口 IProgressHud -----------------------------------------------------
  private
    procedure Show(const aProc: TProc);
    procedure Hide;
    procedure SetText(const aText: string);

  // ---- 私有 -----------------------------------------------------------------
  private
    function O: TControlProgressHud;
  private
    FParentControl: TFmxObject;
    FHudControls: IHudControls;
    FText: string;
    FBackgroundOpacity: Single;
  public
    /// <param name="aParentControl"> 参照用的父容器控件 </param>
    constructor Create(aParentControl: TFmxObject;
      aBackgroundOpacity: Single = 0.1);
    destructor Destroy; override;
  end;



implementation

{ TDialogProgressHud }

constructor TDialogProgressHud.Create(aReferenceForm: TCustomForm;
  aBackgroundOpacity: Single);
begin
  FReferenceForm := aReferenceForm;
  FHudControls := THudControls.Create;
  FBackgroundOpacity := aBackgroundOpacity;
end;

destructor TDialogProgressHud.Destroy;
begin
  if FForm <> nil then
    raise Exception.Create('ProgressHud 对话框，没有结束');
  inherited;
end;

procedure TDialogProgressHud.FormOnClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := TCloseAction.caFree;
end;

procedure TDialogProgressHud.FormOnShow(Sender: TObject);
begin
  if Assigned(FShowProc) then
  begin
    FShowProc();
    FShowProc := nil;
  end;
end;

procedure TDialogProgressHud.Hide;
begin
  if FForm = nil then
    Exit;

  FHudControls.O.Reset;
  FForm.Close;
  // 当关闭时会被释放，因此这里不需要调用析构函数
  FForm := nil;
end;

function TDialogProgressHud.O: TDialogProgressHud;
begin
  Result := Self;
end;

procedure TDialogProgressHud.RootOnMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
  if FForm <> nil then
    FForm.StartWindowDrag;
end;

procedure TDialogProgressHud.SetText(const aText: string);
begin
  FText := aText;
  FHudControls.O.SetText(aText);
end;

procedure TDialogProgressHud.Show(const aProc: TProc);
begin
  if FForm = nil then
  begin
    FForm := TForm.CreateNew(Application);
    FForm.Caption := Application.Title;
    FForm.OnShow := FormOnShow;
    FForm.OnClose := FormOnClose;
    FForm.SetBounds(0, 0, 251, 120);
    FForm.Position := TFormPosition.ScreenCenter;
    //FForm.BorderStyle := TFmxFormBorderStyle.ToolWindow;
    FForm.Transparency := True;

    FHudControls.O.Build(FForm);
    FHudControls.O.Root.Parent := FForm;
    FHudControls.O.Root.OnMouseDown := RootOnMouseDown;
  end;

  // 因为 OnShow 事件可能会产生多次，而只有第一次才是有用的，
  // 而第一次调用完毕后 FShowProc 会被设置为 nil，避免多次调用
  if FReferenceForm = nil then
  begin
    FForm.SetBounds(0, 0, 251, 120);
    FForm.Position := TFormPosition.ScreenCenter;
  end
  else
  begin
    FForm.SetBounds(FReferenceForm.Left, FReferenceForm.Top,
      FReferenceForm.Width, FReferenceForm.Height);
    FForm.Position := TFormPosition.Designed;
  end;
  FShowProc := aProc;
  FHudControls.O.SetBackgroundOpacity(FBackgroundOpacity);
  FHudControls.O.SetText(FText);
  FForm.ShowModal;
end;

{ THudControls }

procedure THudControls.Build(aOwner: TComponent);
var
  mLayout: TLayout;
  mBround: TRectangle;
  mContent: TLayout;
  mAniIndicator: TAniIndicator;
  mText: TText;
begin
  mLayout := TLayout.Create(aOwner);
  mLayout.Align := TAlignLayout.Contents;
  mLayout.HitTest := True;

  mBround := TRectangle.Create(aOwner);
  mBround.Align := TAlignLayout.Contents;
  mBround.Fill.Color := TAlphaColorRec.Null;
  mBround.Stroke.Kind := TBrushKind.None;
  mBround.Opacity := 0.2;
  mBround.Parent := mLayout;
  mBround.HitTest := False;

  mContent := TLayout.Create(aOwner);
  mContent.Align := TAlignLayout.Center;
  mContent.Size.Size := TSizeF.Create(137, 50);
  mContent.Parent := mLayout;
  mContent.HitTest := False;

  mAniIndicator := TAniIndicator.Create(aOwner);
  mAniIndicator.Align := TAlignLayout.Left;
  mAniIndicator.Enabled := True;
  mAniIndicator.Parent := mContent;
  mAniIndicator.HitTest := False;

  mText := TText.Create(aOwner);
  mText.Align := TAlignLayout.Client;
  mText.Margins.Left := 5;
  mText.TextSettings.HorzAlign := TTextAlign.Leading;
  mText.Parent := mContent;
  mText.HitTest := False;
  mText.TextSettings.Font.Family := '微软雅黑';

  SetLength(FControls, 5);
  FControls[0] := mLayout;
  FControls[1] := mBround;
  FControls[2] := mContent;
  FControls[3] := mAniIndicator;
  FControls[4] := mText;

  Root := mLayout;
  Bround := mBround;
  Content := mContent;
  Text := mText;
end;

function THudControls.O: THudControls;
begin
  Result := Self;
end;

procedure THudControls.Reset;
var
  i: Integer;
begin
  try
    if Root <> nil then
      Root.Parent := nil;
  except
  end;

  Root := nil;
  Text := nil;

  for i := High(FControls) downto 0 do
  begin
    try
      FControls[i].Parent := nil;
    except
    end;
  end;

  for i := High(FControls) downto 0 do
  begin
    try
      FControls[i].Free;
      FControls[i] := nil;
    except
      FControls[i] := nil;
    end;
  end;
  SetLength(FControls, 0);
end;

procedure THudControls.SetBackgroundOpacity(aOpacity: Single);
begin
  if (Root = nil) or (Bround = nil) then
    Exit;

  if Bround.Opacity <> aOpacity then
    Bround.Opacity := aOpacity;
end;

procedure THudControls.SetText(const aText: string);
begin
  if (Root = nil) or (Text = nil) or (Content = nil) then
    Exit;

  if aText = '' then
  begin
    Text.Visible := False;
    Content.Size.Width := 50;
    Text.Text := '';
  end
  else
  begin
    Text.Visible := True;
    Content.Size.Width := 150;
    Text.Text := aText;
  end;
end;

{ TControlProgressHud }

constructor TControlProgressHud.Create(aParentControl: TFmxObject;
  aBackgroundOpacity: Single);
begin
  FParentControl := aParentControl;
  FHudControls := THudControls.Create;
  FBackgroundOpacity := aBackgroundOpacity;
end;

destructor TControlProgressHud.Destroy;
begin
  if (FHudControls.O.Root <> nil) then
    raise Exception.Create('ProgressHud 没有结束');
  inherited;
end;

procedure TControlProgressHud.Hide;
begin
  if (FHudControls.O.Root = nil) then
    Exit;

  FHudControls.O.Reset;
end;

function TControlProgressHud.O: TControlProgressHud;
begin
  Result := Self;
end;

procedure TControlProgressHud.SetText(const aText: string);
begin
  FText := aText;
  FHudControls.O.SetText(aText);
end;

procedure TControlProgressHud.Show(const aProc: TProc);
begin
  if (FHudControls.O.Root = nil) then
    FHudControls.O.Build(FParentControl);

  FHudControls.O.SetBackgroundOpacity(FBackgroundOpacity);
  FHudControls.O.SetText(FText);

  FHudControls.O.Root.Parent := FParentControl;

  if Assigned(aProc) then
    aProc();
end;

end.
