unit uRcdf.Arc.Inner;

interface

uses
  System.SysUtils,
  uRcdf.Arc;

type

  // TArc.Cast 函数的转型用 包装类，内部使用
  TArcWarpperForCast<TSrc, T: class> = class(TArcObject, IArcWarpper<T>)
  private
    // 捕获一个引用，避免被释放
    FSrc: IArcWarpper<TSrc>;
    FO: T;
  private
    function O: T;
    function OWarpper: TArcWarpper<T>;
  public
    constructor CreateWarpper(const aObj: IArcWarpper<TSrc>);
    destructor Destroy; override;
    function ToString: string; override;
  end;

implementation

{ TArcWarpperForCast<TSrc, T> }

constructor TArcWarpperForCast<TSrc, T>.CreateWarpper(
  const aObj: IArcWarpper<TSrc>);
begin
  FSrc := aObj;
  FO := FSrc.O as T;
end;

destructor TArcWarpperForCast<TSrc, T>.Destroy;
begin
  FO := nil;
  FSrc := nil;
  inherited;
end;

function TArcWarpperForCast<TSrc, T>.O: T;
begin
  Result := FO;
end;

function TArcWarpperForCast<TSrc, T>.OWarpper: TArcWarpper<T>;
begin
  raise Exception.Create('转型后不支持 .OWarpper');
end;

function TArcWarpperForCast<TSrc, T>.ToString: string;
begin
  Result := FO.ToString;
end;

end.
