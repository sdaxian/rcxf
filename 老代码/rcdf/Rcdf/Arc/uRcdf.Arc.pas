unit uRcdf.Arc;

interface

uses
  System.SysUtils, System.TypInfo, System.Rtti, System.SyncObjs;

type

  // ===========================================================================

  TArcObject = class;
  /// <summary> 自动引用计数(ARC)对象接口 </summary>
  /// <remarks>
  ///   <para>
  ///     因为 Delphi 的接口天生就有自动引用计数能力，因此利用这一特
  ///     点，提供了以 ARC 方式操作类的机制。
  ///   </para><para>
  ///     使用接口对象代替类对象。不要直接使用类对象，而必须使用接口
  ///     对象。通过约定一个 .O 返回接口对应类的函数，可以间接访问类
  ///     的方法和属性等。.O 方法因为返回值对不同的类会不同，因此没法
  ///     使用覆盖的方法实现，因此只是一个约定，且每个接口都要定义一
  ///     个。
  ///   </para><para>
  ///     有两条继承树，分别实际 接口和类。IArcObject 是接口树根对象。
  ///     TArcObject 是类树的跟对象。Arc 接口 和 Arc 类必须一一对应。
  ///   </para><para>
  ///     为了使现存的类也拥有 ARC 能力，提供 IArcWarpper{T} 对其进行
  ///     包装。详细信息参看 IArcWarpper{T}。
  ///   </para><para>
  ///     每个 ARC 接口要定义 IID，这样才可以使用 is, as, Support 等
  ///     操作。
  ///   </para><para>
  ///       TA = class;
  ///       IA = interface(IArcObject)
  ///         [IID]
  ///         function O: TA;
  ///       end;
  ///       TA = class(TArcObject, IA)
  ///       private
  ///         function O: TA;
  ///       end;
  ///   </para>
  /// </remarks>
  /// <author> 孙毅 </author>
  IArcObject = interface
    ['{98398B1F-2559-4DFC-888B-3DBCD4E845CD}']
    /// <summary> 对应的 ArcObject </summary>
    function O: TArcObject;
    /// <summary> 返回被Arc的实际对象，或者值 </summary>
    function GetRttiValue: TValue;
  end;

  /// <summary> ARC 对象 </summary>
  /// <remarks>
  ///   <para>
  ///     因为本类是使用 TInterfacedObject 实现接口的，在其构造函
  ///     数内，引用计数是 1，构造完毕后，引用计数为 0。赋值给接
  ///     口变量，会增加引用计数。如果赋值给类变量则不会改变引用
  ///     计数，因此混用时，有可能会搞乱计数。
  ///   </para><para>
  ///     调用一个使用使用了 const 的接口参数的方法时，如果直接
  ///     把类构造函数传入，则会因为不进行引用计数的处理，而造成
  ///     内存泄露。这种情况下，最后通过一个中间的接口变量进行传
  ///     递，或者使用 as 转一下。
  ///   </para>
  /// </remarks>
  TArcObject = class(TInterfacedObject, IArcObject)
  private
    function O: TArcObject; inline;
  protected
    function GetRttiValue: TValue; virtual;
  public
    /// <summary> 以类型T 返回自己，泛型函数，内部使用 as 关键字 </summary>
    /// <remarks>
    ///   尽量不要使用本方法，而是使用明确类型的 .O 函数，本方法只是
    ///   为了少些ARC类定义代码，提供的，实现上等价于 as 。
    /// </remarks>
    function AsT<T: class>: T;
  end;

  // ---------------------------------------------------------------------------

  /// <summary> 为了方便定义，一个基于泛型的实现，
  ///   一定要注意 T 必须是 自己的实例 的基础类，因为
  //    内部为了效率，使用的是强制转换，编译器是验证
  //    错误写法的 </summary>
  IArcObjectEx<T: class> = interface(IArcObject)
    function O: T;
    {$WARN HIDING_MEMBER OFF}
    property O: T read O;
    {$WARN HIDING_MEMBER DEFAULT}
  end;
  TArcObjectEx<T: class> = class(TArcObject, IArcObjectEx<T>)
  private
    function O: T; inline;
  end;

  // ===========================================================================

  TArcWarpper<T: class> = class;
  /// <summary> ARC 对象泛型接口包装 </summary>
  /// <remarks>
  ///   <para>
  ///     接口包装，用于包装已经存在的类，以提供 ARC 能力。包装不是为
  ///     现有类增加 ARC 接口。而是用 TArcWarpper{T} 泛型类，把被包装
  ///     的类作为泛型类的字段实现的。
  ///   </para><para>
  ///     被包装的类实例的生命期将由 IArcWarpper{T} 控制。
  ///   </para><para>
  ///     实际上每一个提供了 ARC 能力的现有类，都有一个 IArcWarpper{T}
  ///     泛型接口实现和一个 TArcWarpper{T} 泛型类实现。被包装的现有类
  ///     则是 TArcWarpper{T} 泛型类实现的一个私有字段。
  ///   </para><para>
  ///     同样提供 .O 函数，但是 .O 函数返回的是 被包装的类的实例，而
  ///     不是 TArcWarpper{T} 泛型类的实例，有另外的 .OWarpper 返回
  ///     TArcWarpper{T} 泛型类的实例。
  ///   </para><para>
  ///     不要直接使用 TArcWarpper{T} 进行构造，而要通过 TArc.Arc 类方
  ///     法得到 接口实例。
  ///   </para><para>
  ///     因为 Delphi Win32 的泛型接口实例无法定义 IID 的，另外是包装
  ///     方式，所以 如果要基于 T 进行 is as 等转型操作，需要使用专门
  ///     的 .OWarpper.Cast 函数，此函数返回值会是一个与转型后类型对
  ///     应的 IArcWarpper{T} TArcWarpper{T} 实例。
  ///   </para><para>
  ///     不定义 IID 因为如果定义了后，所有的接口实例的 IID 都是相同。
  ///   </para>
  /// </remarks>
  IArcWarpper<T: class> = interface(IArcObject)
    function O: T;
    function OWarpper: TArcWarpper<T>;
  end;

  /// <summary> ARC 对象泛型类包装 </summary>
  TArcWarpper<T: class> = class(TArcObject, IArcWarpper<T>)
  private
    FObj: T;
  private
    function O: T;
    function OWarpper: TArcWarpper<T>;
  protected
    constructor CreateWarpper(aObj: T);
    function GetRttiValue: TValue; override;
  public
    constructor Create();
    destructor Destroy; override;
    function ToString: string; override;
  end;

  // ===========================================================================

  TArcBox<T> = class;
  /// <summary> ARC 箱对象泛型接口 </summary>
  /// <remarks>
  ///   <para>
  ///     本对象用于把如 Integer string double array 等原生类型放到箱
  ///     子里，可以进行装箱和拆箱操作。把这些类型装箱后可以用在一些
  ///     特殊场合
  ///   </para><para>
  ///     箱对象是不变类，不提供修改的操作。
  ///   </para><para>
  ///     不允许放入类实例，因为没有管理生命期会有内存泄露，使用
  ///   </para><para>
  ///     没有必要放入接口实例，因为没有必要。
  ///   </para><para>
  ///     同样提供 .O 函数，但是 .O 函数返回的是箱对象本身。.Value 得
  ///     到，箱里的值。
  ///   </para><para>
  ///     不要直接使用 TArcBox{T} 进行构造，而要通过 TArc.Boxing 类方
  ///     法得到接口实例。
  ///   </para><para>
  ///     因为 Delphi Win32 的泛型接口实例无法定义 IID 的，另外原生类
  ///     型没有必要使用 is as 等转型操作，因此不提供转型函数。
  ///   </para><para>
  ///     不定义 IID 因为如果定义了后，所有的接口实例的 IID 都是相同。
  ///   </para>
  /// </remarks>
  IArcBox<T> = interface(IArcObject)
    function O: TArcBox<T>;
    function Value: T;
  end;

  /// <summary> ARC 箱对象泛型类包装 </summary>
  TArcBox<T> = class(TArcObject, IArcBox<T>)
  private
    FValue: T;
    FToString: string;
    constructor CreateBox(const aValue: T);
  private
    function O: TArcBox<T>;
    function Value: T;
  protected
    function GetRttiValue: TValue; override;
  public
    constructor Create();
    function ToString: string; override;
  end;

  // ===========================================================================

  /// <summary> 自动引用计数辅助类，包含一些类函数 </summary>
  TArc = class sealed(TObject)
  public
    constructor Create();
  public
    /// <summary> 包装对象，返回自动引用计数包装接口 </summary>
    class function Arc<T: class>(aObj: T): IArcWarpper<T>;
    /// <summary> 装箱对象，返回自动引用计数箱接口 </summary>
    class function Box<T>(const aValue: T): IArcBox<T>;

    /// <summary> 包装对象 转型 </summary>
    /// <remarks>
    ///   类似函数如果放在 TArcWarpper 就会出现代码错误现象，因此放到
    ///   这里；虽然使用时麻烦一些。返回值是重新包装的。
    /// </remarks>
    class function Cast<TSrc, TDst: class>(
      const aObject: IArcWarpper<TSrc>): IArcWarpper<TDst>;

    /// <summary> 判断 aSrc 是否是装箱对象且值是否是 T </summary>
    /// <returns>真：装箱对象且值是 T，aDes返回实际装箱类型；
    ///   或假不是 Ts </returns>
    class function SupportBox<T>(const aSrc: IInterface;
      out aDes: IArcBox<T>): Boolean; overload;
    /// <summary> 判断 aSrc 是否是装箱对象且值是否是 T </summary>
    class function SupportBox<T>(const aSrc: IInterface): Boolean; overload;
  end;

implementation

uses uRcdf.Arc.Inner;

{ TArc }

class function TArc.Arc<T>(aObj: T): IArcWarpper<T>;
begin
  if aObj = nil then
    raise Exception.Create('被包装对象不能是 nil');
  Result := TArcWarpper<T>.CreateWarpper(aObj);
end;

class function TArc.Box<T>(const aValue: T): IArcBox<T>;
var
  mPTypeInfo: PTypeInfo;
begin
  // 以后只在 调试模式下进行测试，Release 不测试，以提供性能
  mPTypeInfo := TypeInfo(T);
  if mPTypeInfo.Kind = tkClass then
    raise Exception.Create('被包装类型不能是类对象');

  Result := TArcBox<T>.CreateBox(aValue);
end;

class function TArc.Cast<TSrc, TDst>(
  const aObject: IArcWarpper<TSrc>): IArcWarpper<TDst>;
begin
  Result := TArcWarpperForCast<TSrc, TDst>.CreateWarpper(aObject);
end;

constructor TArc.Create;
begin
  raise Exception.Create('方法类，不允许创建');
end;

//class function TArc.IsT<TOrgin>(const aOrgin: TOrgin;
//  const aTargetTypeInfo: PTypeInfo): Boolean;
//var
//  mOrgin: PTypeInfo;
//  mParent: PTypeData;
//begin
//  mOrgin := TypeInfo(TOrgin);
//
//  if mOrgin = mTarget then
//    Exit(True);
//
//  mParent := GetTypeData(mOrgin);
//  while (mParent <> nil) and (mParent.IntfParent <> nil) do
//  begin
//    if mParent.IntfParent^ = mTarget then
//      Exit(True);
//
//    mParent := GetTypeData(mParent.IntfParent^);
//  end;
//
//  Result := False;
//end;

class function TArc.SupportBox<T>(const aSrc: IInterface): Boolean;
var
  mDes: IArcBox<T>;
begin
  Result := SupportBox<T>(aSrc, mDes);
end;

class function TArc.SupportBox<T>(const aSrc: IInterface;
  out aDes: IArcBox<T>): Boolean;
var
  mArcObject: IArcObject;
begin
  if aSrc = nil then
    Exit(False);

  if not Supports(aSrc, IArcObject, mArcObject) then
    Exit(False);

  if not (mArcObject.O is TArcBox<T>) then
    Exit(False);

  aDes := TArcBox<T>(mArcObject.O);
  Result := True;
end;

{ TArcObject }

function TArcObject.AsT<T>: T;
begin
  Result := Self as T;
end;

function TArcObject.GetRttiValue: TValue;
begin
  Result := TValue.From(O);
end;

function TArcObject.O: TArcObject;
begin
  Result := Self;
end;

{ TArcWarpper<T> }

constructor TArcWarpper<T>.Create;
begin
  raise Exception.Create('不允许直接构造');
end;

constructor TArcWarpper<T>.CreateWarpper(aObj: T);
begin
  FObj := aObj;
end;

destructor TArcWarpper<T>.Destroy;
begin
  FreeAndNil(FObj);
  inherited;
end;

function TArcWarpper<T>.GetRttiValue: TValue;
begin
  Result := TValue.From<T>(O);
end;

function TArcWarpper<T>.O: T;
begin
  Result := FObj;
end;

function TArcWarpper<T>.OWarpper: TArcWarpper<T>;
begin
  Result := Self;
end;

function TArcWarpper<T>.ToString: string;
begin
  Result := FObj.ToString;
end;

{ TArcBox<T> }

constructor TArcBox<T>.Create;
begin
  raise Exception.Create('不允许直接构造');
end;

constructor TArcBox<T>.CreateBox(const aValue: T);
begin
  FValue := aValue;
end;

function TArcBox<T>.GetRttiValue: TValue;
begin
  Result := TValue.From<T>(Value);
end;

function TArcBox<T>.O: TArcBox<T>;
begin
  Result := Self;
end;

function TArcBox<T>.ToString: string;
var
  mStr: string;
  mVal: TValue;
begin
  if FToString = '' then
  begin
    TValue.Make(@FValue, TypeInfo(T), mVal);
    mStr := mVal.ToString;
    FToString := mStr;//TInterlocked.Exchange(FToString, mStr);
  end;

  Result := FToString;
end;

function TArcBox<T>.Value: T;
begin
  Result := FValue;
end;

{ TArcObjectEx<T> }

function TArcObjectEx<T>.O: T;
begin
  // 为了效率使用强制转换
  Result := T(Self);
end;

end.
