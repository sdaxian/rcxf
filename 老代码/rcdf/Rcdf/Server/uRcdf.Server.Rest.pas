unit uRcdf.Server.Rest;

interface

uses
  System.SysUtils, System.Classes, System.JSON, System.Generics.Collections,
  System.Diagnostics, System.Rtti, System.DateUtils, System.NetEncoding,
  REST.Client, REST.Types,

  uRcdf.Arc, uRcdf.Arc.Classes, uRcdf.Error.TError, uRcdf.Error.ErrorCode;

/// <summary>
///    使用 REST.Client 实现的服务调用
/// </summary>

type
  {$SCOPEDENUMS ON}

  TJsonObj = class;
  /// <summary> Json对象的工具类对象 </summary>
  /// <remarks>
  ///   这个对象用于方便获取数据，提供了常用的数据类型解析。
  ///   继承类可以继承以增加对新的数据类型的支持。
  /// </remarks>
  /// <author> 孙毅 </author>
  IJsonObj = interface(IArcObjectEx<TJsonObj>) ['{918AA0BA-DC67-4E20-8245-05F3ACCF023B}'] end;
  TJsonObj = class(TArcObjectEx<TJsonObj>, IJsonObj)
  // ---- 类型定义 -------------------------------------------------------------
  public type
    /// <summary> Json对象数组匿名函数 </summary>
    TJsonObjectArrayProc = reference to procedure(const aJsonObject: TJSONObject);
    /// <summary> 字符串数组匿名函数 </summary>
    TStringArrayProc = reference to procedure(const aStr: string);

    // ---- 私有 -----------------------------------------------------------------
  private
    function TryString(aValue: TJSONValue; out aResult: string): Boolean;

    // ---- 保护 -----------------------------------------------------------------
  protected

    /// <summary> 基于反射的对象字段赋值，被 optObject 方法使用 </summary>
    /// <remarks>
    ///   <para>
    ///     默认只支持基本类型 Boolean, String, Int, Double,
    ///     Long, Date，不支持的类型抛出异常。使用带默认值的
    ///     方式获取Json值。如果要支持更多的类型，请重载。
    ///   </para><para>
    ///     参数都是有效的，不会为空。
    ///   </para>
    /// </remarks>
    /// <param name="aJson"> Json 对象 </param>
    /// <param name="aJsonKey"> Json对象的 Key </param>
    /// <param name="aObject"> 对象实例 </param>
    /// <param name="aField"> 反射的字段 </param>
    procedure SetObjFieldValue(aJson: TJSONObject; const aJsonKey: string;
      aObject: TObject; const aField: TRttiProperty);

    // ---- 公有 方法 ------------------------------------------------------------
  public
    /// <summary> 获取字符串，有且是，否则返回 '' </summary>
    function OptAsString(aJson: TJSONValue): string;
  public
    /// <summary> 获取布尔型，有且是真则返回 真，否则返回假 </summary>
    function OptBoolean(aJson: TJSONObject; const aKey: string): Boolean;
    /// <summary> 获取字符串，有且是，否则返回 '' </summary>
    function OptString(aJson: TJSONObject; const aKey: string): string;
    /// <summary> 获取整数，有且是，否则返回 0 </summary>
    function OptInt(aJson: TJSONObject; const aKey: string): Integer;
    /// <summary> 获取64位整数，有且是，否则返回 0 </summary>
    function OptInt64(aJson: TJSONObject; const aKey: string): Int64;
    /// <summary> 获取浮点数，有且是，否则返回 0.0 </summary>
    function OptDouble(aJson: TJSONObject; const aKey: string): Double;
    /// <summary> 获取日期，有且是Linux时间戳，否则返回 0000.00.00(TDateTime(0)) </summary>
    function OptDate(aJson: TJSONObject; const aKey: string): TDateTime;
    /// <summary> 获取字符串数组，有且是，否则返回 nil </summary>
    function OptStrings(aJson: TJSONObject; const aKey: string): IArcWarpper<TList<string>>;

    /// <summary> 获取数组，有且是，否则返回 nil </summary>
    function OptArray(aJson: TJSONObject; const aKey: string): TJSONArray;
    /// <summary> 获取 Json 对象数组并进行处理，有且是且都处理了，否则返回 False </summary>
    function OptObjectArray(aJson: TJSONObject; const aKey: string;
      const aItemProc: TJsonObjectArrayProc): Boolean;
    /// <summary> 获取 字符串数组并进行处理，有且是且都处理了，否则返回 False </summary>
    function OptStringArray(aJson: TJSONObject; const aKey: string;
      const aItemProc: TStringArrayProc): Boolean;

    /// <summary> 基于反射的对象获取，JSON对象由 aKey 取出
    ///   <remarks>
    ///     使用 setObjFieldValue 进行赋值，如果要支持新的类型，
    ///     重载 setObjFieldValue
    ///   </remarks>
    /// <param name="aJson"> Json 对象 </param>
    /// <param name="aObj"> 对象实例 </param>
    /// <param name="aKeyFields"> Json对象Key 和 对象字段
    ///   的名称对数组，一定是2的倍数 </param>
    procedure OptObject<T: class>(aJson: TJSONObject; const aKey: string;
      aObj: T; const aKeyFields: array of string); overload;

    /// <summary> 基于反射的对象获取，JSON对象就是提供的
    ///   <remarks>
    ///     使用 setObjFieldValue 进行赋值，如果要支持新的类型，
    ///     重载 setObjFieldValue
    ///   </remarks>
    /// </summary>
    /// <param name="aJson"> Json 对象 </param>
    /// <param name="aObj"> 对象实例 </param>
    /// <param name="aKeyFields"> Json对象Key 和 对象字段
    ///   的名称对数组，一定是2的倍数 </param>
    procedure OptObject<T: class>(aJson: TJSONObject; aObj: T;
      const aKeyFields: array of string); overload;
  end;

  // ===========================================================================

  TServerResult = class;
  /// <summary> 服务器请求结果 </summary>
  IServerResult = interface(IArcObjectEx<TServerResult>) ['{94DCAA75-17EF-4F9B-8F71-11C2FE87F414}'] end;
  TServerResult = class sealed(TArcObjectEx<TServerResult>, IServerResult)
  private
    FResponse: TRESTResponse;
    function GetJson: TJSONValue;
    function GetStatusCode: Integer;
    function GetContent: string;
  public
    constructor Create();
    destructor Destroy; override;
  public
    property StatusCode: Integer read GetStatusCode;
    property Json: TJSONValue read GetJson;
    property Content: string read GetContent;
  end;

  // ===========================================================================

  /// <summary>
  ///   重试模式，不重试，重试三次（每次之间间隔 次数*秒 +(-0.5..0.5））
  /// </summary>
  TRetryMode = (NoRelay, ReplyThirce);

  /// <summary> Http 方法 </summary>
  THttpMethod = (Get, Post);

  /// <summary> 服务器访问 </summary>
  TServerAccess = class(TObject)
  private
    FBaseUrl: string;
    procedure SetBaseUrl(const Value: string);

  private
    // 具体执行 REST 的地方
    //   如果执行完毕且没产生异常，根据执行情况返回 真假；
    //   aLastExec: 假，捕获异常返回假，真，有 aError 决定是抛异常还是返回真假
    function DoRest(aRequest: TRESTRequest;
      aError: PtrError; aIgnoreStatus: Boolean;
      aResponse: TRESTResponse; aClient: TRESTClient;
      aRetryMode: TRetryMode; aLastExec: Boolean): Boolean;

    //
    function DoRestUrl(const aBaseUrl: string; const aResource: string;
      const aParams: array of string;
      aError: PtrError = nil; aMethod: THttpMethod = THttpMethod.Get;
      aIgnoreStatus: Boolean = False; aTimeOut: Integer = 30000;
      aRetryMode: TRetryMode = TRetryMode.ReplyThirce): IServerResult;

  public

    /// <summary> REST 查询 </summary>
    /// <param name="aRequest"> 请求 </param>
    /// <param name="aError"> 错误，nil 抛异常 </param>
    /// <param name="aIgnoreStatus"> 忽略 StatueCode 判断 </param>
    /// <param name="aResponse"> 相应，nil 则使用 aRequest.Response ，
    ///   若也为 nil 则会 aRequest 会创建一个生命期由其管理的 Response
    /// </param>
    /// <param name="aClient"> Client 信息，nil 内部会创建一个默认的 </param>
    /// <param name="aRetryMode"> 重试模式 </param>
    procedure Rest(aRequest: TRESTRequest;
      aError: PtrError = nil; aIgnoreStatus: Boolean = False;
      aResponse: TRESTResponse = nil; aClient: TRESTClient = nil;
      aRetryMode: TRetryMode = TRetryMode.ReplyThirce);

    /// <summary>
    ///   使用 URL 和 Resource 进行 REST 查询
    ///   <para>
    ///    默认使用 Get 方式，当 使用 Post 时，将使用
    ///    application/x-www-form-urlencoded 传递参数
    ///   </para>
    /// </summary>
    /// <param name="aBaseUrl"> BaseUrl </param>
    /// <param name="aResource">
    ///   Resource Path，支持 UrlSegment 参数，如果 aParams 名字在 UrlSegment，
    ///   则作为 UrlSegment 参数
    /// </param>
    /// <param name="aParams"> 参数 名值对 数组，名 , 值, 名 , 值, ..... </param>
    /// <param name="aError"> 错误，nil 抛异常 </param>
    /// <param name="aMethod"> HTTP 方法 (GET POST 等) </param>
    /// <param name="aIgnoreStatus"> 忽略 StatueCode 判断 </param>
    /// <param name="aTimeOut"> 超时时间(ms)，每次的超时时间 </param>
    /// <param name="aRetryMode"> 重试模式 </param>
    /// <returns> 结果 </returns>
    function Rest2(const aBaseUrl: string; const aResource: string;
      const aParams: array of string;
      aError: PtrError = nil; aMethod: THttpMethod = THttpMethod.Get;
      aIgnoreStatus: Boolean = False; aTimeOut: Integer = 30000;
      aRetryMode: TRetryMode = TRetryMode.ReplyThirce): IServerResult;


    /// <summary>
    ///   使用 URL 进行 REST 查询
    ///   <para>
    ///    默认使用 Get 方式，当 使用 Post 时，将使用
    ///    application/x-www-form-urlencoded 传递参数
    ///   </para>
    /// </summary>
    /// <param name="aUrl">
    ///   URL，支持 UrlSegment 参数，如果 aParams 名字在 UrlSegment，
    ///   则作为 UrlSegment 参数
    /// </param>
    /// <param name="aParams"> 参数 名值对 数组，名 , 值, 名 , 值, ..... </param>
    /// <param name="aError"> 错误，nil 抛异常 </param>
    /// <param name="aMethod"> HTTP 方法 (GET POST 等) </param>
    /// <param name="aIgnoreStatus"> 忽略 StatueCode 判断 </param>
    /// <param name="aTimeOut"> 超时时间(ms)，每次的超时时间 </param>
    /// <param name="aRetryMode"> 重试模式 </param>
    /// <returns> 结果 </returns>
    function RestUrl(const aUrl: string; const aParams: array of string;
      aError: PtrError = nil; aMethod: THttpMethod = THttpMethod.Get;
      aIgnoreStatus: Boolean = False; aTimeOut: Integer = 30000;
      aRetryMode: TRetryMode = TRetryMode.ReplyThirce): IServerResult;

    /// <summary>
    ///   使用 Resource 进行 REST 查询
    ///   <para>
    ///    默认使用 Get 方式，当 使用 Post 时，将使用
    ///    application/x-www-form-urlencoded 传递参数
    ///   </para>
    /// </summary>
    /// <param name="aResource">
    ///   Resource Path，支持 UrlSegment 参数，如果 aParams 名字在 UrlSegment，
    ///   则作为 UrlSegment 参数
    /// </param>
    /// <param name="aParams"> 参数 名值对 数组，名 , 值, 名 , 值, ..... </param>
    /// <param name="aError"> 错误，nil 抛异常 </param>
    /// <param name="aMethod"> HTTP 方法 (GET POST 等) </param>
    /// <param name="aIgnoreStatus"> 忽略 StatueCode 判断 </param>
    /// <param name="aTimeOut"> 超时时间(ms)，每次的超时时间 </param>
    /// <param name="aRetryMode"> 重试模式 </param>
    /// <returns> 结果 </returns>
    function RestResource(const aResource: string; const aParams: array of string;
      aError: PtrError = nil; aMethod: THttpMethod = THttpMethod.Get;
      aIgnoreStatus: Boolean = False; aTimeOut: Integer = 30000;
      aRetryMode: TRetryMode = TRetryMode.ReplyThirce): IServerResult;

  public
//    /// <summary> 上传(POST，multipart/form-data)，返回 TIdHTTP </summary>
//    /// <param name="aUrl"> URL </param>
//    /// <param name="aFileName"> 文件名 </param>
//    /// <param name="aStream"> 数据流 </param>
//    /// <param name="aTimeOut"> 超时时间(s) </param>
//    /// <returns> TIdHTTP 或 失败 返回 nil </returns>
//    function Upload(const aUrl: string; const aFileName: string;
//      aStream: TStream; aTimeOut: Integer; aError: PtrError): IHttpQueryResult;
//
//    /// <summary> 使用动作上传，返回 Json </summary>
//    /// <param name="aAction"> 动作 ..../aaaaa?... 的 aaaaa 部分 </param>
//    /// <param name="aFileName"> 文件名 </param>
//    /// <param name="aStream"> 数据流 </param>
//    /// <param name="aTimeOut"> 超时时间(s) </param>
//    /// <returns> Json 数据 或 nil 如果失败 </returns>
//    function JsonUpload(const aAction: string; const aFileName: string;
//      aStream: TStream; aTimeOut: Integer; aError: PtrError)
//      : IArcWarpper<TJSONObject>; overload;
//
//    /// <summary> 使用动作上传，返回 Json </summary>
//    /// <param name="aAction"> 动作 ..../aaaaa?... 的 aaaaa 部分 </param>
//    /// <param name="aFileName"> 文件名，'' 使用本地文件名 </param>
//    /// <param name="aLocalFileName"> 本地文件文件名 </param>
//    /// <param name="aTimeOut"> 超时时间(s) </param>
//    /// <returns> Json 数据 或 nil 如果失败 </returns>
//    function JsonUpload(const aAction: string; const aFileName: string;
//      const aLocalFileName: string; aTimeOut: Integer; aError: PtrError)
//      : IArcWarpper<TJSONObject>; overload;
//
//  public
//    /// <summary> 下载，返回 TIdHTTP </summary>
//    /// <param name="aUrl"> URL </param>
//    /// <param name="aTimeOut"> 超时时间(s) </param>
//    /// <returns> TIdHTTP 或 失败 返回 nil </returns>
//    function Download(const aUrl: string; aTimeOut: Integer; aError: PtrError)
//      : IHttpQueryResult;
//
//
//    /// <summary> 下载，返回 流 </summary>
//    /// <param name="aUrl"> URL </param>
//    /// <param name="aTimeOut"> 超时时间(s) </param>
//    /// <returns> 流 或 失败 返回 nil </returns>
//    function StreamDownload(const aUrl: string; aTimeOut: Integer;
//      aError: PtrError): IArcWarpper<TStream>;
  public
    /// <summary> Base Url，配合 Resource 使用 </summary>
    property BaseUrl: string read FBaseUrl write SetBaseUrl;
  end;

implementation

{ TJsonObj }

function TJsonObj.OptAsString(aJson: TJSONValue): string;
begin
  if not TryString(aJson, Result) then
    Result := '';
end;

function TJsonObj.OptArray(aJson: TJSONObject; const aKey: string): TJSONArray;
var
  mValue: TJSONValue;
begin
  mValue := aJson.GetValue(aKey);
  if mValue is TJSONArray then
    Exit(TJSONArray(mValue));
  Result := nil;
end;

function TJsonObj.OptBoolean(aJson: TJSONObject; const aKey: string): Boolean;
var
  mValue: TJSONValue;
begin
  try
    mValue := aJson.GetValue(aKey);
    if mValue = nil then
      Exit(False);

    if mValue is TJSONTrue then
      Exit(True);
    if mValue is TJSONFalse then
      Exit(False);
    if mValue is TJSONNumber then
      Exit(TJSONNumber(mValue).AsInt <> 0);
    if mValue is TJSONString then
      Exit(SameText(TJSONString(mValue).Value, 'true'));

    Result := mValue.GetValue<Boolean>('', False);
  except
    Result := False;
  end;
end;

function TJsonObj.OptDate(aJson: TJSONObject; const aKey: string): TDateTime;
var
  mValue: TJSONValue;
  mInt64: Int64;
  mDate: TDateTime;
  mStr: string;
  mSettings: TFormatSettings;
begin
  try
    mValue := aJson.GetValue(aKey);
    if mValue = nil then
      Exit(0.0);

    if mValue is TJSONNumber then
    begin
      mInt64 := TJSONNumber(mValue).AsInt64;
      mDate := UnixToDateTime(mInt64, True);
      Exit(mDate);
    end;

    if mValue is TJSONString then
    begin
      mStr := TJSONString(mValue).Value;
      if Pos('T',mStr) > 0 then
      begin
        if not TryISO8601ToDate(mStr, mDate, True) then
        begin
          mDate := StrToDateTimeDef( TJSONString(mValue).Value,Now());
        end;
      end
      else
      begin
        mSettings := TFormatSettings.Create;
        mSettings.DateSeparator := '-';
        mSettings.ShortDateFormat := 'yyyy-MM-dd hh:mm:ss';
        if not TryStrToDateTime(mStr,mDate,mSettings) then
        begin
          mSettings.DateSeparator := '/';
          mSettings.ShortDateFormat := 'yyyy/MM/dd hh:mm:ss';
          if not TryStrToDateTime(mStr,mDate,mSettings) then
          begin
            mSettings.DateSeparator := '.';
            mSettings.ShortDateFormat := 'yyyy.MM.dd hh:mm:ss';
            if not TryStrToDateTime(mStr,mDate,mSettings) then
            begin
              mDate := StrToDateTimeDef( TJSONString(mValue).Value,Now());
            end;
          end;
        end;
      end;
      Exit(mDate);
    end;

    Result := mValue.GetValue<TDateTime>('', 0.0);
  except
    Result := 0.0;
  end;
end;

function TJsonObj.OptDouble(aJson: TJSONObject; const aKey: string): Double;
var
  mValue: TJSONValue;
begin
  try
    mValue := aJson.GetValue(aKey);
    if mValue = nil then
      Exit(0.0);

    if mValue is TJSONNumber then
      Exit(TJSONNumber(mValue).AsDouble);

    Result := mValue.GetValue<Double>('', 0.0);
  except
    Result := 0.0;
  end;
end;

function TJsonObj.OptInt(aJson: TJSONObject; const aKey: string): Integer;
var
  mValue: TJSONValue;
  mIntValue: Integer;
begin
  try
    mValue := aJson.GetValue(aKey);
    if mValue = nil then
      Exit(0);

    if mValue is TJSONNumber then
      Exit(TJSONNumber(mValue).AsInt)
    else if mValue is TJSONString then
    begin
      if TJSONString(mValue).Value = '' then
        Exit(0);
      if TryStrToInt(TJSONString(mValue).Value, mIntValue) then
        Exit(mIntValue);
    end;

    Result := mValue.GetValue<Integer>('', 0);
  except
    Result := 0;
  end;
end;

function TJsonObj.OptInt64(aJson: TJSONObject; const aKey: string): Int64;
var
  mValue: TJSONValue;
  mIntValue: Int64;
begin
  try
    mValue := aJson.GetValue(aKey);
    if mValue = nil then
      Exit(0);

    if mValue is TJSONNumber then
      Exit(TJSONNumber(mValue).AsInt64)
    else if mValue is TJSONString then
    begin
      if TJSONString(mValue).Value = '' then
        Exit(0);
      if Int64.TryParse(TJSONString(mValue).Value, mIntValue) then
        Exit(mIntValue);
    end;

    Result := mValue.GetValue<Int64>('', 0);
  except
    Result := 0;
  end;
end;

procedure TJsonObj.OptObject<T>(aJson: TJSONObject; const aKey: string; aObj: T;
  const aKeyFields: array of string);
var
  mJson: TJSONObject;
begin
  mJson := aJson.GetValue(aKey) as TJSONObject;
  OptObject(mJson, aObj, aKeyFields);
end;

procedure TJsonObj.OptObject<T>(aJson: TJSONObject; aObj: T;
  const aKeyFields: array of string);
var
  mRttiContext: TRttiContext;
  mRttiType: TRttiType;
  mRttiProperty: TRttiProperty;
  i, mCount: Integer;
  mJsonKey: string;
  mObjKey: string;
begin
  if (aObj = nil) or (aJson = nil) or (Length(aKeyFields) = 0) or
    (Length(aKeyFields) mod 2 <> 0) then
    raise Exception.Create('参数错误');

  try
    mRttiType := mRttiContext.GetType(aObj.ClassType);

    i := 0;
    mCount := Length(aKeyFields);
    while i < mCount do
    begin
      mJsonKey := aKeyFields[i];
      mObjKey := aKeyFields[i + 1];
      mRttiProperty := mRttiType.GetProperty(mObjKey);
      if mRttiProperty = nil then
        raise Exception.Create('获取对象错误');

      SetObjFieldValue(aJson, mJsonKey, aObj, mRttiProperty);

      Inc(i, 2);
    end;
  except
    raise Exception.Create('获取对象错误');
  end;
end;

function TJsonObj.OptObjectArray(aJson: TJSONObject; const aKey: string;
  const aItemProc: TJsonObjectArrayProc): Boolean;
var
  mJsonArray: TJSONArray;
  i: Integer;
  mJsonObj: TJSONObject;
begin
  try
    mJsonArray := aJson.GetValue(aKey) as TJSONArray;
    for i := 0 to mJsonArray.Count - 1 do
    begin
      mJsonObj := mJsonArray.Items[i] as TJSONObject;
      aItemProc(mJsonObj);
    end;
    Result := True;
  except
    Result := False;
  end;
end;

function TJsonObj.OptStringArray(aJson: TJSONObject; const aKey: string;
  const aItemProc: TStringArrayProc): Boolean;
var
  mJsonArray: TJSONArray;
  i: Integer;
  mJsonValue: TJSONValue;
  mS: string;
begin
  try
    mJsonArray := aJson.GetValue(aKey) as TJSONArray;
    for i := 0 to mJsonArray.Count - 1 do
    begin
      mJsonValue := mJsonArray.Items[i];
      if not TryString(mJsonValue, mS) then
        mS := '';
      aItemProc(mS);
    end;
    Result := True;
  except
    Result := False;
  end;
end;

function TJsonObj.OptString(aJson: TJSONObject; const aKey: string): string;
var
  mValue: TJSONValue;
begin
  mValue := aJson.GetValue(aKey);
  if not TryString(mValue, Result) then
    Exit('');
end;

function TJsonObj.OptStrings(aJson: TJSONObject; const aKey: string)
  : IArcListString;
var
  mValue: TJSONValue;
  i: Integer;
  mItem: TJSONValue;
  mS: string;
begin
  try
    mValue := aJson.GetValue(aKey);
    if mValue = nil then
      Exit(nil);

    if not(mValue is TJSONArray) then
      Exit(nil);

    Result := TArc.Arc(TList<string>.Create());
    Result.O.Capacity := TJSONArray(mValue).Count;

    for i := 0 to TJSONArray(mValue).Count do
    begin
      mItem := TJSONArray(mValue).Items[i];
      if not TryString(mItem, mS) then
        Exit(nil);

      Result.O.Add(mS);
    end;
  except
    Result := nil;
  end;
end;

procedure TJsonObj.SetObjFieldValue(aJson: TJSONObject; const aJsonKey: string;
  aObject: TObject; const aField: TRttiProperty);
var
  mType: TRttiType;
begin
  if aField = nil then
    raise Exception.Create('没有提供或不支持的类型 !');
  mType := aField.PropertyType;
  if mType = nil then
    raise Exception.Create('没有提供或不支持的类型 !');

  try
    if mType.Handle = TypeInfo(Integer) then
      aField.SetValue(aObject, OptInt(aJson, aJsonKey))
    else if mType.Handle = TypeInfo(Double) then
      aField.SetValue(aObject, OptDouble(aJson, aJsonKey))
    else if mType.Handle = TypeInfo(Int64) then
      aField.SetValue(aObject, OptInt64(aJson, aJsonKey))
    else if mType.Handle = TypeInfo(Boolean) then
      aField.SetValue(aObject, OptBoolean(aJson, aJsonKey))
    else if mType.Handle = TypeInfo(string) then
      aField.SetValue(aObject, OptString(aJson, aJsonKey))
    else if mType.Handle = TypeInfo(TDateTime) then
      aField.SetValue(aObject, OptDate(aJson, aJsonKey))
    else
      raise Exception.Create('没有提供或不支持的列类型 !');
  except
    raise Exception.Create('没有提供或不支持的列类型 !');
  end;
end;

function TJsonObj.TryString(aValue: TJSONValue; out aResult: string): Boolean;
begin
  try
    if aValue = nil then
      Exit(False);

    if aValue is TJSONString then
    begin
      aResult := TJSONString(aValue).Value;
      Exit(True);
    end;

    if aValue is TJSONArray then
    begin
      aResult := TJSONArray(aValue).ToJSON;
      Exit(True);
    end;

    aResult := aValue.GetValue<string>('');
    Result := True;
  except
    Result := False;
  end;
end;

{ TServerResult }

constructor TServerResult.Create;
begin
  FResponse := TRESTResponse.Create(nil);
end;

destructor TServerResult.Destroy;
begin
  FreeAndNil(FResponse);
  inherited;
end;

function TServerResult.GetContent: string;
begin
  Result := FResponse.Content;
end;

function TServerResult.GetJson: TJSONValue;
begin
  Result := FResponse.JSONValue;
//    mFormatError := False;
//    if mContent = '' then
//      mFormatError := True
//    else
//    begin
//      mFirstContent := mContent.Substring(0, 20);
//      mFirstContent := mFirstContent.Trim;
//      mFirstContent := mContent.Substring(0, 6);
//      mFirstContent := mFirstContent.ToLower;
//      if SameText(mFirstContent, '<html>') then
//        mFormatError := True
//    end;
//
//    if not mFormatError then
//    begin
//      mJsonValue := TArc.Arc(TJSONObject.ParseJSONValue(mContent));
//      if (mJsonValue = nil) or (not(mJsonValue.O is TJSONObject)) then
//        mFormatError := True
//      else
//        Result := TArc.Cast<TJSONValue, TJSONObject>(mJsonValue);
//    end;
//
//    if mFormatError then
//    begin
//      TError.SetError(aError, TError.Create(ecHttpFormatError));
//      Exit(nil);
//    end;
end;

function TServerResult.GetStatusCode: Integer;
begin
  Result := FResponse.StatusCode;
end;

//{ THttpQueryResult }
//
//constructor THttpQueryResult.Create;
//begin
//  FResponse := TRESTResponse.Create(nil);
//end;
//
//destructor THttpQueryResult.Destroy;
//begin
//  FreeAndNil(FResponse);
//  inherited;
//end;
//
//function THttpQueryResult.GetJsonValue: TJSONValue;
//begin
//
//end;
//
//function THttpQueryResult.GetResponseCode: Integer;
//begin
//
//end;
//
//procedure THttpQueryResult.SetFileName(aFileName: string);
//begin
//  FFileName := Copy(aFileName,pos('filename=',aFileName)+ 9,10000 );
//end;

{ TServerAccess }

function TServerAccess.DoRest(aRequest: TRESTRequest;
  aError: PtrError; aIgnoreStatus: Boolean;
  aResponse: TRESTResponse; aClient: TRESTClient;
  aRetryMode: TRetryMode; aLastExec: Boolean): Boolean;
var
  mRequest: TCustomRESTRequest;
  mResponse: TCustomRESTResponse;
  mClient: TCustomRESTClient;
  mError: IError;
begin
  mRequest := aRequest;

  mResponse := aResponse;
  if mResponse = nil then
  begin
    if mRequest.Response <> nil then
      mResponse := mRequest.Response;
  end;
  if mRequest.Response <> mResponse then
    mRequest.Response := mResponse;


  mClient := aClient;
  if mClient = nil then
  begin
    if mRequest.Client <> nil then
      mClient := mRequest.Client
    else
      mClient := TRESTClient.Create('');
  end;
  if mRequest.Client <> mClient then
    mRequest.Client := mClient;

  try
    try
      mRequest.Execute();
      mResponse := mRequest.Response;

      if (not aIgnoreStatus) and (not mResponse.Status.SuccessOK_200()) then
      begin
        mError := TError.Create(ecHttpQueryError);

        if aError = nil then
          raise EErrorException.CreateError(mError);
        aError^ := mError;

        Exit(False);
      end;

      Exit(True);

    except
      on E: Exception do
      begin
        mError := TError.Create(ecHttpQueryError, E);

        if aError = nil then
          raise EErrorException.CreateError(mError);
        aError^ := mError;

        Exit(False);
      end;
    end;
  finally
    if mClient <> aClient then
      FreeAndNil(mClient);
  end;
end;

function TServerAccess.DoRestUrl(const aBaseUrl, aResource: string;
  const aParams: array of string; aError: PtrError; aMethod: THttpMethod;
  aIgnoreStatus: Boolean; aTimeOut: Integer;
  aRetryMode: TRetryMode): IServerResult;
var
  mRequest: IArcWarpper<TRESTRequest>;
  mClient: IArcWarpper<TRESTClient>;
  i: Integer;
  mName: string;
  mValue: string;
  mParam: TRESTRequestParameter;
begin
  mRequest := TArc.Arc(TRESTRequest.Create(nil));
  mClient := TArc.Arc(TRESTClient.Create(aBaseUrl));

  mRequest.O.Resource := aResource;
  if aTimeOut > 0 then
    mRequest.O.Timeout := aTimeOut;

  // mIdHttp.O.Request.CacheControl := 'no-cache';
  // mIdHttp.O.Request.ContentType := 'application/x-www-form-urlencoded';

  if aMethod = THttpMethod.Get then
    mRequest.O.Method := TRESTRequestMethod.rmGET
  else
    mRequest.O.Method := TRESTRequestMethod.rmPOST;

  if Length(aParams) > 0 then
  begin
    for i := 0 to Length(aParams) div 2 - 1 do
    begin
      mName := aParams[i * 2 + 0];
      mValue := aParams[i * 2 + 1];

      mParam := mRequest.O.Params.ParameterByName(mName);
      if mParam <> nil then
        mParam.Value := mValue
      else if mName = '__BODY__' then
        mRequest.O.Params.AddItem(mName, mValue, TRESTRequestParameterKind.pkREQUESTBODY)
      else if mName = '__JSON_BODY__' then
        mRequest.O.Params.AddItem(mName, mValue, TRESTRequestParameterKind.pkREQUESTBODY,
          [], TRESTContentType.ctAPPLICATION_JSON)
      else
        mRequest.O.AddParameter(mName, mValue);
    end;
  end;

  Result := TServerResult.Create;
  Rest(mRequest.O, aError, aIgnoreStatus, Result.O.FResponse, mClient.O, aRetryMode);
end;

procedure TServerAccess.Rest(aRequest: TRESTRequest;
  aError: PtrError; aIgnoreStatus: Boolean;
  aResponse: TRESTResponse; aClient: TRESTClient;
  aRetryMode: TRetryMode);
var
  mExecCount: Integer;
  mLastExec: Boolean;
  mRestRet: Boolean;
  mSleep: Integer;
begin
  mExecCount := 1;

  while True do
  begin
    // 先去掉 在最大超时时间内可能尝试不超过三次 的逻辑

    mLastExec := True;
    if (aRetryMode = TRetryMode.ReplyThirce) and (mExecCount < 3) then
      mLastExec := False;

    mRestRet := DoRest(aRequest, aError, aIgnoreStatus,
      aResponse, aClient, aRetryMode, mLastExec);
    if mRestRet then
      Exit;

    if mLastExec then
    begin
      if aError = nil then
        raise EErrorException.CreateError(TError.Create(ecHttpTimeOut));

      if TError.HasError(aError) then
        Exit;

      TError.SetError(aError, TError.Create(ecHttpTimeOut));
      Exit;
    end;

    // 每次之间重试次数*秒数 + (-0.5 .. 0.5)
    mSleep := mExecCount * 1000 + Random(1000) - 500;
    if mSleep < 0 then
      mSleep := 217;

    TThread.Sleep(mSleep);
    Inc(mExecCount);
  end;
end;

function TServerAccess.Rest2(const aBaseUrl, aResource: string;
  const aParams: array of string; aError: PtrError; aMethod: THttpMethod;
  aIgnoreStatus: Boolean; aTimeOut: Integer;
  aRetryMode: TRetryMode): IServerResult;
begin
  Result := DoRestUrl(aBaseUrl, aResource, aParams, aError, aMethod,
    aIgnoreStatus, aTimeOut, aRetryMode);
end;

function TServerAccess.RestResource(const aResource: string;
  const aParams: array of string; aError: PtrError; aMethod: THttpMethod;
  aIgnoreStatus: Boolean; aTimeOut: Integer;
  aRetryMode: TRetryMode): IServerResult;
begin
  Result := DoRestUrl(FBaseUrl, aResource, aParams, aError, aMethod,
    aIgnoreStatus, aTimeOut, aRetryMode);
end;

function TServerAccess.RestUrl(const aUrl: string; const aParams: array of string;
  aError: PtrError; aMethod: THttpMethod;
  aIgnoreStatus: Boolean; aTimeOut: Integer;
  aRetryMode: TRetryMode): IServerResult;
begin
  Result := DoRestUrl('', aUrl, aParams, aError, aMethod, aIgnoreStatus,
    aTimeOut, aRetryMode);
end;

procedure TServerAccess.SetBaseUrl(const Value: string);
begin
 FBaseUrl := Value;
end;

initialization

end.
