unit uRcdf.Server;

interface

uses
  System.SysUtils, System.Classes, System.JSON, System.Generics.Collections,
  System.Diagnostics, System.Rtti, System.DateUtils, System.NetEncoding,
  IdHTTP, IdGlobal, IdMultipartFormData, IdExceptionCore,
  uRcdf.Arc, uRcdf.Arc.Classes, uRcdf.Error.TError, uRcdf.Error.ErrorCode;

var
  FTestUiStopwatch: TStopwatch;
  FTestServerTimeList: TThreadList<Int64>;

type

  /// <summary> 列表匿名函数 </summary>
  /// <remarks>
  /// <para>
  /// 用于服务器管理器获取列表出的数据内容，数据要包括一个作为ID
  /// 使用的内容和其它任意内容。
  /// </para><para>
  /// 数据内容在调用函数内全部遍历中只使用此实例。
  /// </para>
  /// </remarks>
  /// <author>孙毅</author>
  TListProc<T: IArcObject> = reference to procedure(const aId: string;
    const aObj: T);

  TJsonObj = class;

  IJsonObj = interface(IArcObject)
    ['{5F1587F6-0248-4AB1-892A-CBB8A65BC693}']
    function O: TJsonObj;
  end;

  /// <summary> Json对象的包装 </summary>
  /// <remarks>
  /// 这个对象用于方便获取数据，提供了常用的数据类型解析。
  /// 继承类可以继承以增加对新的数据类型的支持。
  /// </remarks>
  /// <author> 孙毅 </author>
  TJsonObj = class(TArcObject, IJsonObj)
    // ---- 类型定义 -------------------------------------------------------------
  public type
    /// <summary> Json对象数组 函数 </summary>
    TJsonObjectArrayProc = reference to procedure(const aJsonObject
      : TJSONObject);
    /// <summary> 字符串数组 函数 </summary>
    TStringArrayProc = reference to procedure(const aStr: string);

    // ---- 私有 -----------------------------------------------------------------
  private
    function O: TJsonObj;
    function TryString(aValue: TJSONValue; out aResult: string): Boolean;

    // ---- 保护 -----------------------------------------------------------------
  protected
    /// <summary> 基于反射的对象字段赋值，被 optObject 方法使用 </summary>
    /// <remarks>
    /// <para>
    /// 默认只支持基本类型 Boolean, String, Int, Double,
    /// Long, Date，不支持的类型抛出异常。使用带默认值的
    /// 方式获取Json值。如果要支持更多的类型，请重载。
    /// </para><para>
    /// 参数都是有效的，不会为空。
    /// </para>
    /// </remarks>
    /// <param name="aJson"> Json 对象 </param>
    /// <param name="aJsonKey"> Json对象的 Key </param>
    /// <param name="aObject"> 对象实例 </param>
    /// <param name="aField"> 反射的字段 </param>
    procedure SetObjFieldValue(aJson: TJSONObject; const aJsonKey: string;
      aObject: TObject; const aField: TRttiProperty);

    // ---- 公有 方法 ------------------------------------------------------------
  public
    /// <summary> 获取字符串，有且是，否则返回 '' </summary>
    function OptAsString(aJson: TJSONValue): string;
  public
    /// <summary> 获取布尔型，有且是真则返回 真，否则返回假 </summary>
    function OptBoolean(aJson: TJSONObject; const aKey: string): Boolean;
    /// <summary> 获取字符串，有且是，否则返回 '' </summary>
    function OptString(aJson: TJSONObject; const aKey: string): string;
    /// <summary> 获取整数，有且是，否则返回 0 </summary>
    function OptInt(aJson: TJSONObject; const aKey: string): Integer;
    /// <summary> 获取64位整数，有且是，否则返回 0 </summary>
    function OptInt64(aJson: TJSONObject; const aKey: string): Int64;
    /// <summary> 获取浮点数，有且是，否则返回 0.0 </summary>
    function OptDouble(aJson: TJSONObject; const aKey: string): Double;
    /// <summary> 获取日期，有且是Linux时间戳，否则返回 0000.00.00(TDateTime(0)) </summary>
    function OptDate(aJson: TJSONObject; const aKey: string): TDateTime;
    /// <summary> 获取字符串数组，有且是，否则返回 nil </summary>
    function OptStrings(aJson: TJSONObject; const aKey: string)
      : IArcWarpper<TList<string>>;

    /// <summary> 获取数组，有且是，否则返回 nil </summary>
    function OptArray(aJson: TJSONObject; const aKey: string): TJSONArray;

    /// <summary> 获取 Json 对象数组并进行处理，有且是且都处理了，否则返回 False </summary>
    function OptObjectArray(aJson: TJSONObject; const aKey: string;
      const aItemProc: TJsonObjectArrayProc): Boolean;

    /// <summary> 获取 字符串数组并进行处理，有且是且都处理了，否则返回 False </summary>
    function OptStringArray(aJson: TJSONObject; const aKey: string;
      const aItemProc: TStringArrayProc): Boolean;

    /// <summary> 基于反射的对象获取，JSON对象由 aKey 取出
    /// <remarks>
    /// 使用 setObjFieldValue 进行赋值，如果要支持新的类型，
    /// 重载 setObjFieldValue
    /// </remarks>
    /// <param name="aJson"> Json 对象 </param>
    /// <param name="aObj"> 对象实例 </param>
    /// <param name="aKeyFields"> Json对象Key 和 对象字段
    /// 的名称对数组，一定是2的倍数 </param>
    procedure OptObject<T: class>(aJson: TJSONObject; const aKey: string;
      aObj: T; const aKeyFields: array of string); overload;

    /// <summary> 基于反射的对象获取，JSON对象就是提供的
    /// <remarks>
    /// 使用 setObjFieldValue 进行赋值，如果要支持新的类型，
    /// 重载 setObjFieldValue
    /// </remarks>
    /// <param name="aJson"> Json 对象 </param>
    /// <param name="aObj"> 对象实例 </param>
    /// <param name="aKeyFields"> Json对象Key 和 对象字段
    /// 的名称对数组，一定是2的倍数 </param>
    procedure OptObject<T: class>(aJson: TJSONObject; aObj: T;
      const aKeyFields: array of string); overload;
  end;

  // ===========================================================================

  THttpQueryResult = class;

  IHttpQueryResult = interface(IArcObject)
    ['{04F80D16-4CCD-49FB-9D03-BEB409269A94}']
    function O: THttpQueryResult;
  end;

  /// <summary> 查询 Http 结果 </summary>
  THttpQueryResult = class sealed(TArcObject, IHttpQueryResult)
  private
    function O: THttpQueryResult;
  private
    FStream: TStream;
    FResponseCode: Integer;
    FFileName: string;
  protected
    procedure SetResponseCode(aResponseCode: Integer);
  public
    constructor Create();
    destructor Destroy; override;
  public
    property ResponseCode: Integer read FResponseCode;
    property Stream: TStream read FStream;
    property FileName: string read FFileName;
  public
    procedure SetFileName(aFileName: string);
    function AsString(aEncode: TEncoding): string;
    // 使用此方法后，Stream 不在有效，变为 nil 了
    // 返回的 Stream 的位置是开始位置
    function AsStream(): IArcWarpper<TStream>;
  end;

  // ===========================================================================

  /// <summary> 服务器访问 </summary>
  TServerAccess = class(TObject)
  private
    FActionUrl: string;
    procedure SetActionUrl(const Value: string);
    procedure SetDefStringEncoding(Sender: TObject);
  public
    /// <summary> 查询 http 一次(POST)，返回 TIdHTTP </summary>
    /// <param name="aUrl"> URL </param>
    /// <param name="aParams"> 参数 名值对 数组，名 , 值, 名 , 值, ..... </param>
    /// <param name="aTimeOut"> 超时时间(s) </param>
    /// <param name="aIgnoreResponseCode"> 忽略 ResponseCode 判断 </param>
    /// <returns> TIdHTTP 或 失败 返回 nil </returns>
    function HttpQueryOnce(const aUrl: string; const aParams: array of string;
      aTimeOut: Integer; aIgnoreResponseCode: Boolean; aError: PtrError)
      : IHttpQueryResult;

    /// <summary> 查询 http (POST)，在超时时间内可能尝试不超过三次，
    /// 返回 TIdHTTP </summary>
    /// <param name="aUrl"> URL </param>
    /// <param name="aParams"> 参数 名值对 数组，名 , 值, 名 , 值, ..... </param>
    /// <param name="aTimeOut"> 超时时间(s) </param>
    /// <param name="aIgnoreResponseCode"> 忽略 ResponseCode 判断 </param>
    /// <returns> TIdHTTP 或 失败 返回 nil </returns>
    function HttpQuery(const aUrl: string; const aParams: array of string;
      aTimeOut: Integer; aIgnoreResponseCode: Boolean; aError: PtrError)
      : IHttpQueryResult; overload;

    /// <summary> 查询 http (POST)，返回 TIdHTTP </summary>
    /// <param name="aUrl"> URL </param>
    /// <param name="aParams"> 参数 名值对 数组，名 , 值, 名 , 值, ..... </param>
    /// <param name="aTimeOut"> 超时时间(s) </param>
    /// <returns> TIdHTTP 或 失败 返回 nil </returns>
    function HttpQuery(const aUrl: string; const aParams: array of string;
      aTimeOut: Integer; aError: PtrError): IHttpQueryResult; overload;

  public
    /// <summary> 使用 URL 查询 http (POST)，返回 字符串 </summary>
    /// <param name="aUrl">  </param>
    /// <param name="aParams"> 参数 名值对 数组，名 , 值, 名 , 值, ..... </param>
    /// <param name="aTimeOut"> 超时时间(s) </param>
    /// <returns> 成功 : 真 </returns>
    function StringHttpQueryByUrl(const aUrl: string;
      const aParams: array of string; aTimeOut: Integer; aEncoding: TEncoding;
      out aString: string; aError: PtrError): Boolean;

    function StringHttpQueryByAction(const aAction: string;
      const aParams: array of string; aTimeOut: Integer; aEncoding: TEncoding;
      out aString: string; aError: PtrError): Boolean;

  public
    /// <summary> 使用 URL 查询 http (POST)，返回 Json </summary>
    /// <remarks> 注意：数字等类型也可能是以字符串形式 </remarks>
    /// <param name="aUrl">  </param>
    /// <param name="aParams"> 参数 名值对 数组，名 , 值, 名 , 值, ..... </param>
    /// <param name="aTimeOut"> 超时时间(s) </param>
    /// <returns> Json 数据 或 失败 返回 nil </returns>
    function JsonHttpQueryByUrl(const aUrl: string;
      const aParams: array of string; aTimeOut: Integer; aError: PtrError)
      : IArcWarpper<TJSONObject>;

    /// <summary> 使用动作查询 http (POST)，返回 Json </summary>
    /// <remarks>
    /// <para>
    /// 注意：数字等类型也可能是以字符串形式
    /// </para><para>
    /// 使用 BaseUrl 和 动作 已经生成 URL
    /// </para>
    /// </remarks>
    // @param aAction 动作 ..../aaaaa?... 的 aaaaa 部分
    // @param aParams 参数 名值对 数组，名 , 值, 名 , 值, .....
    // @param aTimeOut 超时时间(s)
    // @return Json 数据 或 失败 返回 nil
    function JsonHttpQuery(const aAction: string;
      const aParams: array of string; aTimeOut: Integer; aError: PtrError)
      : IArcWarpper<TJSONObject>;

  public
    /// <summary> 上传(POST，multipart/form-data)，返回 TIdHTTP </summary>
    /// <param name="aUrl"> URL </param>
    /// <param name="aFileName"> 文件名 </param>
    /// <param name="aStream"> 数据流 </param>
    /// <param name="aTimeOut"> 超时时间(s) </param>
    /// <returns> TIdHTTP 或 失败 返回 nil </returns>
    function Upload(const aUrl: string; const aFileName: string;
      aStream: TStream; aTimeOut: Integer; aError: PtrError): IHttpQueryResult;

    /// <summary> 使用动作上传，返回 Json </summary>
    /// <param name="aAction"> 动作 ..../aaaaa?... 的 aaaaa 部分 </param>
    /// <param name="aFileName"> 文件名 </param>
    /// <param name="aStream"> 数据流 </param>
    /// <param name="aTimeOut"> 超时时间(s) </param>
    /// <returns> Json 数据 或 nil 如果失败 </returns>
    function JsonUpload(const aAction: string; const aFileName: string;
      aStream: TStream; aTimeOut: Integer; aError: PtrError)
      : IArcWarpper<TJSONObject>; overload;

    /// <summary> 使用动作上传，返回 Json </summary>
    /// <param name="aAction"> 动作 ..../aaaaa?... 的 aaaaa 部分 </param>
    /// <param name="aFileName"> 文件名，'' 使用本地文件名 </param>
    /// <param name="aLocalFileName"> 本地文件文件名 </param>
    /// <param name="aTimeOut"> 超时时间(s) </param>
    /// <returns> Json 数据 或 nil 如果失败 </returns>
    function JsonUpload(const aAction: string; const aFileName: string;
      const aLocalFileName: string; aTimeOut: Integer; aError: PtrError)
      : IArcWarpper<TJSONObject>; overload;

  public
    /// <summary> 下载，返回 TIdHTTP </summary>
    /// <param name="aUrl"> URL </param>
    /// <param name="aTimeOut"> 超时时间(s) </param>
    /// <returns> TIdHTTP 或 失败 返回 nil </returns>
    function Download(const aUrl: string; aTimeOut: Integer; aError: PtrError)
      : IHttpQueryResult;


    /// <summary> 下载，返回 流 </summary>
    /// <param name="aUrl"> URL </param>
    /// <param name="aTimeOut"> 超时时间(s) </param>
    /// <returns> 流 或 失败 返回 nil </returns>
    function StreamDownload(const aUrl: string; aTimeOut: Integer;
      aError: PtrError): IArcWarpper<TStream>;
  public
    /// <summary> 动作 Url，配合 JsonHttpQuery 等方法使用 </summary>
    property ActionUrl: string read FActionUrl write SetActionUrl;
  end;

  TServerUtils = class abstract(TObject)
  public
    constructor Create();
  public
    // 逗号分隔
    class function CommaStringFromStrings(const aStrs: TArray<string>;const ASplit : string = ','): string;
  end;

implementation

{ TJsonObj }

function TJsonObj.O: TJsonObj;
begin
  Result := Self;
end;

function TJsonObj.OptAsString(aJson: TJSONValue): string;
begin
  if not TryString(aJson, Result) then
    Result := '';
end;

function TJsonObj.OptArray(aJson: TJSONObject; const aKey: string): TJSONArray;
var
  mValue: TJSONValue;
begin
  mValue := aJson.GetValue(aKey);
  if mValue is TJSONArray then
    Exit(TJSONArray(mValue));
  Result := nil;
end;

function TJsonObj.OptBoolean(aJson: TJSONObject; const aKey: string): Boolean;
var
  mValue: TJSONValue;
begin
  try
    mValue := aJson.GetValue(aKey);
    if mValue = nil then
      Exit(False);

    if mValue is TJSONTrue then
      Exit(True);
    if mValue is TJSONFalse then
      Exit(False);
    if mValue is TJSONNumber then
      Exit(TJSONNumber(mValue).AsInt <> 0);
    if mValue is TJSONString then
      Exit(SameText(TJSONString(mValue).Value, 'true'));

    Result := mValue.GetValue('', False);
  except
    Result := False;
  end;
end;

function TJsonObj.OptDate(aJson: TJSONObject; const aKey: string): TDateTime;
var
  mValue: TJSONValue;
  mInt64: Int64;
  mDate: TDateTime;
  mStr: string;
  mSettings: TFormatSettings;
begin
  try
    mValue := aJson.GetValue(aKey);
    if mValue = nil then
      Exit(0.0);

    if mValue is TJSONNumber then
    begin
      mInt64 := TJSONNumber(mValue).AsInt64;
      mDate := UnixToDateTime(mInt64, True);
      Exit(mDate);
    end;

    if mValue is TJSONString then
    begin
      mStr := TJSONString(mValue).Value;
      if Pos('T',mStr) > 0 then
      begin
        if not TryISO8601ToDate(mStr, mDate, True) then
        begin
          mDate := StrToDateTimeDef( TJSONString(mValue).Value,Now());
        end;
      end
      else
      begin
        mSettings := TFormatSettings.Create;
        mSettings.DateSeparator := '-';
        mSettings.ShortDateFormat := 'yyyy-MM-dd hh:mm:ss';
        if not TryStrToDateTime(mStr,mDate,mSettings) then
        begin
          mSettings.DateSeparator := '/';
          mSettings.ShortDateFormat := 'yyyy/MM/dd hh:mm:ss';
          if not TryStrToDateTime(mStr,mDate,mSettings) then
          begin
            mSettings.DateSeparator := '.';
            mSettings.ShortDateFormat := 'yyyy.MM.dd hh:mm:ss';
            if not TryStrToDateTime(mStr,mDate,mSettings) then
            begin
              mDate := StrToDateTimeDef( TJSONString(mValue).Value,Now());
            end;
          end;
        end;
      end;
      Exit(mDate);
    end;

    Result := mValue.GetValue('', 0.0);
  except
    Result := 0.0;
  end;
end;

function TJsonObj.OptDouble(aJson: TJSONObject; const aKey: string): Double;
var
  mValue: TJSONValue;
begin
  try
    mValue := aJson.GetValue(aKey);
    if mValue = nil then
      Exit(0.0);

    if mValue is TJSONNumber then
      Exit(TJSONNumber(mValue).AsDouble);

    Result := mValue.GetValue<Double>('', 0.0);
  except
    Result := 0.0;
  end;
end;

function TJsonObj.OptInt(aJson: TJSONObject; const aKey: string): Integer;
var
  mValue: TJSONValue;
  mIntValue: Integer;
begin
  try
    mValue := aJson.GetValue(aKey);
    if mValue = nil then
      Exit(0);

    if mValue is TJSONNumber then
      Exit(TJSONNumber(mValue).AsInt)
    else if mValue is TJSONString then
    begin
      if TJSONString(mValue).Value = '' then
        Exit(0);
      if TryStrToInt(TJSONString(mValue).Value, mIntValue) then
        Exit(mIntValue);
    end;

    Result := mValue.GetValue('', 0);
  except
    Result := 0;
  end;
end;

function TJsonObj.OptInt64(aJson: TJSONObject; const aKey: string): Int64;
var
  mValue: TJSONValue;
  mIntValue: Int64;
begin
  try
    mValue := aJson.GetValue(aKey);
    if mValue = nil then
      Exit(0);

    if mValue is TJSONNumber then
      Exit(TJSONNumber(mValue).AsInt64)
    else if mValue is TJSONString then
    begin
      if TJSONString(mValue).Value = '' then
        Exit(0);
      if Int64.TryParse(TJSONString(mValue).Value, mIntValue) then
        Exit(mIntValue);
    end;

    Result := mValue.GetValue('', 0);
  except
    Result := 0;
  end;
end;

procedure TJsonObj.OptObject<T>(aJson: TJSONObject; const aKey: string; aObj: T;
  const aKeyFields: array of string);
var
  mJson: TJSONObject;
begin
  mJson := aJson.GetValue(aKey) as TJSONObject;
  OptObject(mJson, aObj, aKeyFields);
end;

procedure TJsonObj.OptObject<T>(aJson: TJSONObject; aObj: T;
  const aKeyFields: array of string);
var
  mRttiContext: TRttiContext;
  mRttiType: TRttiType;
  mRttiProperty: TRttiProperty;
  i, mCount: Integer;
  mJsonKey: string;
  mObjKey: string;
begin
  if (aObj = nil) or (aJson = nil) or (Length(aKeyFields) = 0) or
    (Length(aKeyFields) mod 2 <> 0) then
    raise Exception.Create('参数错误');

  try
    mRttiType := mRttiContext.GetType(aObj.ClassType);

    i := 0;
    mCount := Length(aKeyFields);
    while i < mCount do
    begin
      mJsonKey := aKeyFields[i];
      mObjKey := aKeyFields[i + 1];
      mRttiProperty := mRttiType.GetProperty(mObjKey);
      if mRttiProperty = nil then
        raise Exception.Create('获取对象错误');

      SetObjFieldValue(aJson, mJsonKey, aObj, mRttiProperty);

      Inc(i, 2);
    end;
  except
    raise Exception.Create('获取对象错误');
  end;
end;

function TJsonObj.OptObjectArray(aJson: TJSONObject; const aKey: string;
  const aItemProc: TJsonObjectArrayProc): Boolean;
var
  mJsonArray: TJSONArray;
  i: Integer;
  mJsonObj: TJSONObject;
begin
  try
    mJsonArray := aJson.GetValue(aKey) as TJSONArray;
    for i := 0 to mJsonArray.Count - 1 do
    begin
      mJsonObj := mJsonArray.Items[i] as TJSONObject;
      aItemProc(mJsonObj);
    end;
    Result := True;
  except
    Result := False;
  end;
end;

function TJsonObj.OptStringArray(aJson: TJSONObject; const aKey: string;
  const aItemProc: TStringArrayProc): Boolean;
var
  mJsonArray: TJSONArray;
  i: Integer;
  mJsonValue: TJSONValue;
  mS: string;
begin
  try
    mJsonArray := aJson.GetValue(aKey) as TJSONArray;
    for i := 0 to mJsonArray.Count - 1 do
    begin
      mJsonValue := mJsonArray.Items[i];
      if not TryString(mJsonValue, mS) then
        mS := '';
      aItemProc(mS);
    end;
    Result := True;
  except
    Result := False;
  end;
end;

function TJsonObj.OptString(aJson: TJSONObject; const aKey: string): string;
var
  mValue: TJSONValue;
begin
  mValue := aJson.GetValue(aKey);
  if not TryString(mValue, Result) then
    Exit('');
end;

function TJsonObj.OptStrings(aJson: TJSONObject; const aKey: string)
  : IArcListString;
var
  mValue: TJSONValue;
  i: Integer;
  mItem: TJSONValue;
  mS: string;
begin
  try
    mValue := aJson.GetValue(aKey);
    if mValue = nil then
      Exit(nil);

    if not(mValue is TJSONArray) then
      Exit(nil);

    Result := TArc.Arc(TList<string>.Create());
    Result.O.Capacity := TJSONArray(mValue).Count;

    for i := 0 to TJSONArray(mValue).Count do
    begin
      mItem := TJSONArray(mValue).Items[i];
      if not TryString(mItem, mS) then
        Exit(nil);

      Result.O.Add(mS);
    end;
  except
    Result := nil;
  end;
end;

procedure TJsonObj.SetObjFieldValue(aJson: TJSONObject; const aJsonKey: string;
  aObject: TObject; const aField: TRttiProperty);
var
  mType: TRttiType;
begin
  if aField = nil then
    raise Exception.Create('没有提供或不支持的类型 !');
  mType := aField.PropertyType;
  if mType = nil then
    raise Exception.Create('没有提供或不支持的类型 !');

  try
    if mType.Handle = TypeInfo(Integer) then
      aField.SetValue(aObject, OptInt(aJson, aJsonKey))
    else if mType.Handle = TypeInfo(Double) then
      aField.SetValue(aObject, OptDouble(aJson, aJsonKey))
    else if mType.Handle = TypeInfo(Int64) then
      aField.SetValue(aObject, OptInt64(aJson, aJsonKey))
    else if mType.Handle = TypeInfo(Boolean) then
      aField.SetValue(aObject, OptBoolean(aJson, aJsonKey))
    else if mType.Handle = TypeInfo(string) then
      aField.SetValue(aObject, OptString(aJson, aJsonKey))
    else if mType.Handle = TypeInfo(TDateTime) then
      aField.SetValue(aObject, OptDate(aJson, aJsonKey))
    else
      raise Exception.Create('没有提供或不支持的列类型 !');
  except
    raise Exception.Create('没有提供或不支持的列类型 !');
  end;
end;

function TJsonObj.TryString(aValue: TJSONValue; out aResult: string): Boolean;
begin
  try
    if aValue = nil then
      Exit(False);

    if aValue is TJSONString then
    begin
      aResult := TJSONString(aValue).Value;
      Exit(True);
    end;

    if aValue is TJSONArray then
    begin
      aResult := TJSONArray(aValue).ToJSON;
      Exit(True);
    end;

    aResult := aValue.GetValue<string>('');
    Result := True;
  except
    Result := False;
  end;
end;

{ THttpQueryResult }

function THttpQueryResult.AsStream: IArcWarpper<TStream>;
begin
  if FStream = nil then
    raise Exception.Create('已经 AsStream 过了');
  FStream.Position := 0;
  Result := TArc.Arc(FStream);
  FStream := nil;
end;

function THttpQueryResult.AsString(aEncode: TEncoding): string;
begin
  FStream.Position := 0;
  Result := ReadStringFromStream(FStream, -1, IndyTextEncoding(aEncode));
end;
procedure THttpQueryResult.SetFileName(aFileName: string);
begin
  FFileName := Copy(aFileName,pos('filename=',aFileName)+ 9,10000 );
end;

constructor THttpQueryResult.Create;
begin
  FStream := TMemoryStream.Create;
end;

destructor THttpQueryResult.Destroy;
begin
  FreeAndNil(FStream);
  inherited;
end;

function THttpQueryResult.O: THttpQueryResult;
begin
  Result := Self
end;

procedure THttpQueryResult.SetResponseCode(aResponseCode: Integer);
begin
  FResponseCode := aResponseCode;
end;

{ TServerAccess }

function TServerAccess.HttpQuery(const aUrl: string;
  const aParams: array of string; aTimeOut: Integer;
  aIgnoreResponseCode: Boolean; aError: PtrError): IHttpQueryResult;
var
  mStart: Cardinal;
  mTimeEnd: Cardinal;
  mTryCount: Integer;
begin
  mStart := TThread.GetTickCount;
  mTimeEnd := aTimeOut * 1000;
  mTryCount := 0;
  while True do
  begin
    Result := HttpQueryOnce(aUrl, aParams, aTimeOut,
      aIgnoreResponseCode, aError);
    if Result <> nil then
      Exit;

    if TThread.GetTickCount - mStart >= mTimeEnd then
      Exit(TError.SetError<IHttpQueryResult>(nil, aError,
        TError.Create(ecHttpTimeOut)));

    if mTryCount >= 3 then
    begin
      if TError.HasError(aError) then
        Exit(nil)
      else
        Exit(TError.SetError<IHttpQueryResult>(nil, aError,
          TError.Create(ecHttpTimeOut)));
    end;

    Inc(mTryCount);

    // 每次之间休息 217毫秒
    TThread.Sleep(217);
  end;
end;

procedure TServerAccess.SetDefStringEncoding(Sender: TObject);
begin
  TIdHTTP(Sender).IOHandler.DefStringEncoding := IndyTextEncoding_OSDefault;
end;


function TServerAccess.Download(const aUrl: string; aTimeOut: Integer;
  aError: PtrError): IHttpQueryResult;
var
  mIdHttp: IArcWarpper<TIdHTTP>;
begin
  // 是否在这里判断是否主线程和网络是否有效？？待思考
  mIdHttp := TArc.Arc(TIdHTTP.Create(nil));
  try
    mIdHttp.O.HandleRedirects := True;
    mIdHttp.O.OnConnected := SetDefStringEncoding;

    mIdHttp.O.Request.AcceptLanguage := 'zh-cn';
    mIdHttp.O.ReadTimeout := aTimeOut * 1000;

    Result := THttpQueryResult.Create;
    mIdHttp.O.Get(aUrl, Result.O.Stream);
    Result.O.SetResponseCode(mIdHttp.O.Response.ResponseCode);
    Result.O.SetFileName(mIdHttp.O.Response.ContentDisposition);
    if Result.O.ResponseCode <> 200 then
    begin
      if Result.O.ResponseCode = 404 then
        TError.SetError(aError, TError.Create(ecHttpNotFound))
      else
        TError.SetError(aError, TError.Create(ecHttpDownloadFail));
      Result := nil;
    end;

  except
    on E: Exception do
    begin
      // 超时错误也应该在这判断
      TError.SetError(aError, TError.Create(ecHttpQueryError, E));
      Result := nil;
    end;
  end;
end;

function TServerAccess.HttpQuery(const aUrl: string;
  const aParams: array of string; aTimeOut: Integer; aError: PtrError)
  : IHttpQueryResult;
begin
  Result := HttpQuery(aUrl, aParams, aTimeOut, False, aError);
end;

function TServerAccess.HttpQueryOnce(const aUrl: string;
  const aParams: array of string; aTimeOut: Integer;
  aIgnoreResponseCode: Boolean; aError: PtrError): IHttpQueryResult;
var
  mIdHttp: IArcWarpper<TIdHTTP>;
  mSb: IArcStringBuilder;
  mCount: Integer;
  i: Integer;
  mKey: string;
  mValue: string;
  mValueEncode: string;
  mStringStream: IArcWarpper<TStringStream>;
//  mWatch: TStopwatch;
begin
//  mWatch := TStopwatch.Create;
//  mWatch.Start;

  // 是否在这里判断是否主线程和网络是否有效？？待思考
  //
//  TLog.MEnterExit(lolWorn, lolError, Format('执行%s', [aUrl]));
//  TLog.O(lolWorn, Format('开始时间:%s',
//    [FormatDateTime('yyyy-mm-dd HH:MM:SS', Now)]));
  mIdHttp := TArc.Arc(TIdHTTP.Create(nil));
  try
    mIdHttp.O.HandleRedirects := True;
    // mIdHttp.O.ConnectTimeout := 60 * 1000;
    mIdHttp.O.ReadTimeout := aTimeOut * 1000;
    mIdHttp.O.Request.CacheControl := 'no-cache';
    mIdHttp.O.Request.ContentType := 'application/x-www-form-urlencoded';

    if Length(aParams) > 0 then
    begin
      mSb := TArcMake.MakeStringBuilder(512);
      mCount := Length(aParams) div 2;
      for i := 0 to mCount - 1 do
      begin
        mKey := aParams[i * 2 + 0];
        mValue := aParams[i * 2 + 1];
        mValueEncode := TNetEncoding.URL.Encode(mValue);

        if i > 0 then
          mSb.O.Append('&');
        mSb.O.Append(mKey).Append('=').Append(mValueEncode);
      end;

      mStringStream := TArc.Arc(TStringStream.Create(mSb.O.ToString,
        TEncoding.UTF8));
    end;

    if mStringStream = nil then
      mStringStream := TArc.Arc(TStringStream.Create('', TEncoding.UTF8));

    Result := THttpQueryResult.Create;
//    TLog.O(lolDebug, Format('%s&%s', [aUrl, mStringStream.O.DataString]));
    mIdHttp.O.Post(aUrl, mStringStream.O, Result.O.Stream);
//    TLog.O(lolDebug, Result.O.AsString(TEncoding.UTF8));
    Result.O.SetResponseCode(mIdHttp.O.Response.ResponseCode);

    if (not aIgnoreResponseCode) and (Result.O.ResponseCode <> 200) then
    begin
      TError.SetError(aError, TError.Create(ecHttpQueryError));
      Result := nil;
    end;
//    mWatch.Stop;
//    FTestServerTimeList.Add(mWatch.ElapsedMilliseconds);
//    TLog.O(lolWorn, Format('执行结束,共用 %d 毫秒', [mWatch.ElapsedMilliseconds]));

  except
    on E: EIdReadTimeout do
    begin
      TError.SetError(aError, TError.Create(ecHttpTimeOut, E));
      Result := nil;
    end;
    on E: Exception do
    begin
      TError.SetError(aError, TError.Create(ecHttpQueryError, E));
      Result := nil;
    end;
  end;
end;

function TServerAccess.JsonHttpQuery(const aAction: string;
  const aParams: array of string; aTimeOut: Integer; aError: PtrError)
  : IArcJSONObject;
var
  mUrl: string;
begin
  if aAction = '' then
    mUrl := FActionUrl
  else
    mUrl := FActionUrl + aAction;

  Result := JsonHttpQueryByUrl(mUrl, aParams, aTimeOut, aError);
end;

function TServerAccess.JsonHttpQueryByUrl(const aUrl: string;
  const aParams: array of string; aTimeOut: Integer; aError: PtrError)
  : IArcJSONObject;
var
  mHttpQueryResult: IHttpQueryResult;
  mContent: string;
  mFirstContent: string;
  mFormatError: Boolean;
  mJsonValue: IArcWarpper<TJSONValue>;
begin
  mHttpQueryResult := HttpQuery(aUrl, aParams, aTimeOut, aError);
  if (mHttpQueryResult = nil) then
  begin
    if not TError.HasError(aError) then
      TError.SetError(aError, TError.Create(ecHttpQueryError));
    Exit(nil);
  end;

  try
    mContent := mHttpQueryResult.O.AsString(TEncoding.UTF8);

    mFormatError := False;
    if mContent = '' then
      mFormatError := True
    else
    begin
      mFirstContent := mContent.Substring(0, 20);
      mFirstContent := mFirstContent.Trim;
      mFirstContent := mContent.Substring(0, 6);
      mFirstContent := mFirstContent.ToLower;
      if SameText(mFirstContent, '<html>') then
        mFormatError := True
    end;

    if not mFormatError then
    begin
      mJsonValue := TArc.Arc(TJSONObject.ParseJSONValue(mContent));
      if (mJsonValue = nil) or (not(mJsonValue.O is TJSONObject)) then
        mFormatError := True
      else
        Result := TArc.Cast<TJSONValue, TJSONObject>(mJsonValue);
    end;

    if mFormatError then
    begin
      TError.SetError(aError, TError.Create(ecHttpFormatError));
      Exit(nil);
    end;
  except
    on E: Exception do
    begin
      TError.SetError(aError, TError.Create(ecHttpFormatError, E));
      Exit(nil);
    end;
  end;
end;

function TServerAccess.JsonUpload(const aAction, aFileName, aLocalFileName
  : string; aTimeOut: Integer; aError: PtrError): IArcWarpper<TJSONObject>;
var
  mStream: IArcWarpper<TIdReadFileExclusiveStream>;
  mFileName: string;
begin
  mStream := TArc.Arc(TIdReadFileExclusiveStream.Create(aLocalFileName));
  mFileName := aFileName;
  if mFileName = '' then
    mFileName := aLocalFileName;
  Result := JsonUpload(aAction, mFileName, mStream.O, aTimeOut, aError);
end;

function TServerAccess.JsonUpload(const aAction, aFileName: string;
  aStream: TStream; aTimeOut: Integer; aError: PtrError)
  : IArcWarpper<TJSONObject>;
var
  mUrl: string;
  mHttpQueryResult: IHttpQueryResult;
  mContent: string;
  mFirstContent: string;
  mFormatError: Boolean;
  mJsonValue: IArcWarpper<TJSONValue>;
begin
  if aAction = '' then
    mUrl := FActionUrl
  else
    mUrl := FActionUrl + aAction;

  mHttpQueryResult := Upload(mUrl, aFileName, aStream, aTimeOut, aError);
  if (mHttpQueryResult = nil) then
  begin
    if not TError.HasError(aError) then
      TError.SetError(aError, TError.Create(ecHttpUploadFail));
    Exit(nil);
  end;

  try
    mContent := mHttpQueryResult.O.AsString(TEncoding.UTF8);

    mFormatError := False;
    if mContent = '' then
      mFormatError := True
    else
    begin
      mFirstContent := mContent.Substring(0, 20);
      mFirstContent := mFirstContent.Trim;
      mFirstContent := mContent.Substring(0, 6);
      mFirstContent := mFirstContent.ToLower;
      if SameText(mFirstContent, '<html>') then
        mFormatError := True
    end;

    if not mFormatError then
    begin
      mJsonValue := TArc.Arc(TJSONObject.ParseJSONValue(mContent));
      if (mJsonValue = nil) or (not(mJsonValue.O is TJSONObject)) then
        mFormatError := True
      else
        Result := TArc.Cast<TJSONValue, TJSONObject>(mJsonValue);
    end;

    if mFormatError then
    begin
      TError.SetError(aError, TError.Create(ecHttpFormatError));
      Exit(nil);
    end;
  except
    on E: Exception do
    begin
      TError.SetError(aError, TError.Create(ecHttpFormatError, E));
      Exit(nil);
    end;
  end;
end;

procedure TServerAccess.SetActionUrl(const Value: string);
var
  mActionUrl: string;
begin
  mActionUrl := Value;
  if mActionUrl.Length > 0 then
  begin
    if mActionUrl[mActionUrl.Length] <> '/' then
      mActionUrl := mActionUrl + '/';
  end;

  FActionUrl := mActionUrl;
end;

function TServerAccess.StreamDownload(const aUrl: string; aTimeOut: Integer;
  aError: PtrError): IArcWarpper<TStream>;
var
  mHttpQueryResult: IHttpQueryResult;
begin
  mHttpQueryResult := Download(aUrl, aTimeOut, aError);
  if (mHttpQueryResult = nil) then
  begin
    if not TError.HasError(aError) then
      TError.SetError(aError, TError.Create(ecHttpUploadFail));
    Exit(nil);
  end;

  try
    Result := mHttpQueryResult.O.AsStream;
  except
    on E: Exception do
    begin
      TError.SetError(aError, TError.Create(ecHttpFormatError, E));
      Exit(nil);
    end;
  end;
end;

function TServerAccess.StringHttpQueryByUrl(const aUrl: string;
  const aParams: array of string; aTimeOut: Integer; aEncoding: TEncoding;
  out aString: string; aError: PtrError): Boolean;
var
  mHttpQueryResult: IHttpQueryResult;
  mContent: string;
begin
  aString := '';

  mHttpQueryResult := HttpQuery(aUrl, aParams, aTimeOut, aError);
  if (mHttpQueryResult = nil) then
  begin
    if not TError.HasError(aError) then
      TError.SetError(aError, TError.Create(ecHttpQueryError));
    Exit(False);
  end;

  try
    if aEncoding = nil then
      aEncoding := TEncoding.UTF8;
    mContent := mHttpQueryResult.O.AsString(aEncoding);

    aString := mContent;
    Result := True;
  except
    on E: Exception do
    begin
      TError.SetError(aError, TError.Create(ecHttpFormatError, E));
      Exit(False);
    end;
  end;
end;

function TServerAccess.StringHttpQueryByAction(const aAction: string;
      const aParams: array of string; aTimeOut: Integer; aEncoding: TEncoding;
      out aString: string; aError: PtrError): Boolean;
  var
  mUrl: string;
begin
  if aAction = '' then
    mUrl := FActionUrl
  else
    mUrl := FActionUrl + aAction;
  Result := StringHttpQueryByUrl(mUrl,aParams,aTimeOut,aEncoding,aString,aError);
end;

function TServerAccess.Upload(const aUrl, aFileName: string; aStream: TStream;
  aTimeOut: Integer; aError: PtrError): IHttpQueryResult;
var
  mIdHttp: IArcWarpper<TIdHTTP>;
  mStream: IArcWarpper<TIdMultiPartFormDataStream>;
begin
  // 是否在这里判断是否主线程和网络是否有效？？待思考

  mIdHttp := TArc.Arc(TIdHTTP.Create(nil));
  try
    mIdHttp.O.HandleRedirects := True;
    // mIdHttp.O.ConnectTimeout := 60 * 1000;
    mIdHttp.O.ReadTimeout := aTimeOut * 1000;

    mStream := TArc.Arc(TIdMultiPartFormDataStream.Create);
    mStream.O.AddFormField('file', '', '', aStream, aFileName);

    Result := THttpQueryResult.Create;
    mIdHttp.O.Post(aUrl, mStream.O, Result.O.Stream);
    Result.O.SetResponseCode(mIdHttp.O.Response.ResponseCode);

    if Result.O.ResponseCode <> 200 then
    begin
      TError.SetError(aError, TError.Create(ecHttpQueryError));
      Result := nil;
    end;

  except
    on E: Exception do
    begin
      // 超时错误也应该在这判断
      TError.SetError(aError, TError.Create(ecHttpQueryError, E));
      Result := nil;
    end;
  end;
end;

{ TServerUtils }

class function TServerUtils.CommaStringFromStrings(const aStrs: TArray<string>;const ASplit : string = ','): string;
var
  mLength: Integer;
  mSb: IArcStringBuilder;
  i: Integer;
begin
  mLength := System.Length(aStrs);
  if mLength = 0 then
    Result := ''
  else
  begin
    mSb := TArcMake.MakeStringBuilder(mLength * 20);
    mSb.O.Append(aStrs[0]);
    for i := 1 to mLength - 1 do
      mSb.O.Append(ASplit).Append(aStrs[i]);
    Result := mSb.O.ToString;
  end;
end;

constructor TServerUtils.Create;
begin
  raise Exception.Create('工具类，不能创建！');
end;

initialization

FTestUiStopwatch := TStopwatch.Create;
FTestServerTimeList := TThreadList<Int64>.Create;

end.
