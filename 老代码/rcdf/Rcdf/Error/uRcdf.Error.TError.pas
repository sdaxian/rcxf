unit uRcdf.Error.TError;

interface

uses
  System.SysUtils,
  uRcdf.Arc, uRcdf.Error.ErrorCode;

type

  TExceptionInfo = class(TObject)
  private
    FExceptionMessage: string;
    FExceptionClassName: string;
  public
    property ExceptionClassName: string read FExceptionClassName;
    property ExceptionMessage: string read FExceptionMessage;
  end;


  TError = class;
  PtrError = ^IError;   // PError System 有同名的，容易造成误解
  IError = interface(IArcObject)
    ['{054A9FAD-C5D1-422B-92BF-0C302D1430F2}']
    function O: TError;
  end;

  /// <summary>
  /// 错误.
  /// * 本框架或者使用本框架的应用，使用本类或子类描述错误。如果被
  /// * 实例化，则说明发生了错误，并且本类可以含有错误的信息。
  /// * 使用本框架的应用为了描述自己的错误信息，可以继承本类，定义
  /// * 自己的错误代码和其它信息，这是错误代码应该使用 Custom
  /// * 表示使用子类自己定义的错误代码。
  /// * @author 孙毅
  ///</summary>
  TError = class(TArcObject, IError)
  private
    function O: TError;
  private
    FCode: TErrorCode;
    FMessage: string ;
    FError: TExceptionInfo;
  public
    ///** 辅助函数，设置错误 */
    class function SetError<T>(const aResult: T; aErrorRtr: PtrError;
      aError: IError): T; overload;
    ///** 辅助函数，设置错误 */
    class procedure SetError(aErrorRtr: PtrError; aError: IError); overload;

    ///** 辅助函数，设置错误 */
    class function SetErrorIfNil<T>(const aResult: T; aErrorRtr: PtrError;
      aError: IError): T; overload;
    ///** 辅助函数，设置错误 */
    class procedure SetErrorIfNil(aErrorRtr: PtrError; aError: IError); overload;

    ///** 辅助函数，是否有错误, 有且设置了错误，返回真，否则返回假 */
    class function HasError(aErrorRtr: PtrError): Boolean; overload;
    ///** 辅助函数，是否有错误, 有且设置了错误，返回真，否则返回假 */
    class function HasError(aError: IError): Boolean; overload;

  public
    constructor Create(aCode: TErrorCode; const aMessage: string; aError: Exception); overload;
    constructor Create(aCode: TErrorCode; const aMessage: string); overload;
    constructor Create(aCode: TErrorCode; const aMessageFmt: string; const aArgs: array of const); overload;
    constructor Create(aCode: TErrorCode; aError: Exception); overload;
    constructor Create(aCode: TErrorCode); overload;
  public
    function ToString: string; override;

    /// <summary> 错误代码 </summary>
    property Code: TErrorCode read FCode;
    /// <summary> 错误信息 </summary>
    property &Message: string read FMessage;
    /// <summary> 异常 </summary>
    property Error: TExceptionInfo read FError;

  public
    class function MakeError<T>(const aResult: T; var aError: IError; aInError: IError): T; overload;
    class function MakeError<T>(const aResult: T; var aError: IError; aCode: TErrorCode; const aMessage: string; aException: Exception): T; overload;
    class function MakeError<T>(const aResult: T; var aError: IError; aCode: TErrorCode; const aMessage: string): T; overload;
    class function MakeError<T>(const aResult: T; var aError: IError; aCode: TErrorCode; aException: Exception): T; overload;
    class function MakeError<T>(const aResult: T; var aError: IError; aCode: TErrorCode): T; overload;

    class function MakeError<T>(const aResult: T; var aError: IError; aCode: TErrorCode; const aMessageFmt: string; const aArgs: array of const): T; overload;
    class function MakeError<T>(const aResult: T; var aError: IError; aCode: TErrorCode; const aMessageFmt: string; const aArgs: array of const; aException: Exception): T; overload;
  end;

  /// <summary>
  ///   在不使用 Error 的地方，会抛出这个异常
  /// </summary>
  EErrorException = class(Exception)
  private
    FError: IError;
  public
    constructor CreateError(aError: IError);
  public
    property Error: IError read FError;
  end;

implementation

{ TRError }

constructor TError.Create(aCode: TErrorCode; const aMessage: string);
begin
  Create(aCode, aMessage, nil);
end;

constructor TError.Create(aCode: TErrorCode; const aMessage: string;
  aError: Exception);
begin
  FCode := aCode;
  FMessage := aMessage;
  if aError = nil then
    FError := nil
  else
  begin
    FError := TExceptionInfo.Create;
    FError.FExceptionMessage := aError.Message;
    FError.FExceptionClassName := aError.ClassName
  end;
end;

constructor TError.Create(aCode: TErrorCode);
begin
  Create(aCode, '', nil);
end;

function TError.O: TError;
begin
  Result := Self;
end;

constructor TError.Create(aCode: TErrorCode; const aMessageFmt: string;
  const aArgs: array of const);
begin
  Create(aCode, Format(aMessageFmt, aArgs), nil);
end;

class function TError.HasError(aError: IError): Boolean;
begin
  Result := aError <> nil
end;

class function TError.MakeError<T>(const aResult: T; var aError: IError;
  aCode: TErrorCode; const aMessage: string; aException: Exception): T;
begin
  aError := TError.Create(aCode, aMessage, aException);
  Result := aResult;
end;

class function TError.MakeError<T>(const aResult: T; var aError: IError;
  aCode: TErrorCode; const aMessage: string): T;
begin
  aError := TError.Create(aCode, aMessage);
  Result := aResult;
end;

class function TError.MakeError<T>(const aResult: T; var aError: IError;
  aCode: TErrorCode; aException: Exception): T;
begin
  aError := TError.Create(aCode, aException);
  Result := aResult;
end;

class function TError.MakeError<T>(const aResult: T; var aError: IError;
  aCode: TErrorCode): T;
begin
  aError := TError.Create(aCode);
  Result := aResult;
end;

class function TError.MakeError<T>(const aResult: T; var aError: IError;
  aInError: IError): T;
begin
  aError := aInError;
  Result := aResult;
end;

class function TError.HasError(aErrorRtr: PtrError): Boolean;
begin
  Result := (aErrorRtr <> nil) and HasError(aErrorRtr^)
end;

class procedure TError.SetError(aErrorRtr: PtrError; aError: IError);
begin
  if aErrorRtr <> nil then
    aErrorRtr^ := aError;
end;

class function TError.SetError<T>(const aResult: T; aErrorRtr: PtrError;
  aError: IError):T;
begin
  if aErrorRtr <> nil then
    aErrorRtr^ := aError;
  Result := aResult;
end;

class procedure TError.SetErrorIfNil(aErrorRtr: PtrError; aError: IError);
begin
  if (aErrorRtr = nil) or (aErrorRtr^ <> nil) then
    Exit;
  SetError(aErrorRtr, aError);
end;

class function TError.SetErrorIfNil<T>(const aResult: T; aErrorRtr: PtrError;
  aError: IError): T;
begin
  if (aErrorRtr = nil) or (aErrorRtr^ <> nil) then
    Exit(aResult);
  Result := SetError(aResult, aErrorRtr, aError);
end;

function TError.ToString: string;
var
  mMsg1: string;
  mMsg2: string;
begin
  mMsg1 := FMessage;
  if (FError <> nil) then
  begin
    mMsg2 := FError.FExceptionMessage;
    if mMsg2.IsEmpty then
      if not FError.FExceptionClassName.IsEmpty then
        mMsg2 := FError.FExceptionClassName + '异常';

    if not mMsg2.IsEmpty then
    begin
      if not mMsg1.IsEmpty then
        mMsg1 := mMsg1 + '，';
      mMsg1 := mMsg1 + mMsg2;
    end;
  end;

  if mMsg1.IsEmpty then
    Result := Format('%.8X', [Ord(FCode)])
  else
    Result := Format('%.8X, %s', [Ord(FCode), mMsg1]);
end;

constructor TError.Create(aCode: TErrorCode; aError: Exception);
begin
  Create(aCode, '', aError);
end;


class function TError.MakeError<T>(const aResult: T; var aError: IError;
  aCode: TErrorCode; const aMessageFmt: string; const aArgs: array of const): T;
begin
  aError := TError.Create(aCode, Format(aMessageFmt, aArgs));
  Result := aResult;
end;

class function TError.MakeError<T>(const aResult: T; var aError: IError;
  aCode: TErrorCode; const aMessageFmt: string; const aArgs: array of const;
  aException: Exception): T;
begin
  aError := TError.Create(aCode, Format(aMessageFmt, aArgs), aException);
  Result := aResult;
end;

{ EErrorException }

constructor EErrorException.CreateError(aError: IError);
begin
  FError := aError;
end;

end.
