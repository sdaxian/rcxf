unit uRcdf.Error.ErrorCode;
//  Rcdf
//
//  Created by 孙毅 on 14-10-27.
/// <summary>
{ 错误代码.
 *
 *  本框架使用的错误代码。
 *
 *  使用本框架的应用可以定义自己的错误代码，但是建议错误代码值统一
 *  使用高位为一的32位整数。即 $800000000。
 *
 *  错误代码值按数字大小可以分成多个区域，每个区域都是在
 *  $80000000 - $8900000 之间，这样大概有16个区域。<br>
 *  其中第一个区域 $800000000 - $810000000 被本框架使用。<br>
 *  其它区域可以被使用本框架应用用于定义自己的错误代码值。
 *
 *  这里提供了其它区域的基础值 ragionBaseX ，其它值可以在基础值上
 *  进行计算（减法，因为 Java 没有 无符号整数，$80000000 是
 *  负数，所以用减法）得出。
 *
 *  swift 枚举无法使用 1+1 这样的形式，只能写成 $80000000 的形式
 *
 *  @author
 }
///</summary>
/// <author>孙毅</author>

interface

type
  /// <summary>区域代码基础值(0..9)</summary>
  TErrorCodeRagion =
  (
    recrBase0 = -2147483648 or $0000000, // $80000000,
    recrBase1 = -2147483648 or $1000000, // $81000000,
    recrBase2 = -2147483648 or $2000000, // $82000000,
    recrBase3 = -2147483648 or $3000000, // $83000000,
    recrBase4 = -2147483648 or $4000000, // $84000000,
    recrBase5 = -2147483648 or $5000000, // $85000000,
    recrBase6 = -2147483648 or $6000000, // $86000000,
    recrBase7 = -2147483648 or $7000000, // $87000000,
    recrBase8 = -2147483648 or $8000000, // $88000000,
    recrBase9 = -2147483648 or $9000000 // $89000000
  );

  TErrorCode =
  (
     /// <summary>内部错误 </summary>
     ecInnerError   = Ord(recrBase0) or 0,
     /// <summary>其它错误 </summary>
     ecOtherError   = Ord(recrBase0) or 1,
     /// <summary>定制错误，使用本框架的应用定制了自己错误代码  </summary>
     ecCustom       = Ord(recrBase0) or 2,
     /// <summary>参数错误 </summary>
     ecParamError   = Ord(recrBase0) or 3,
     /// <summary>数据太多，一般发生在列表新数据时，新数据太多，不能全部获取</summary>
     ecDataTooMuch  = Ord(recrBase0) or 4,
     /// <summary> 与数据库有关的错误 </summary>
     ecDataBaseError = Ord(recrBase0) or 5,

     /// <summary> 查询Http错误 </summary>
     ecHttpQueryError   = Ord(recrBase0) or $1000 or 0,
     /// <summary> 查询Http超时 </summary>
     ecHttpTimeOut      = Ord(recrBase0) or $1000 or 1,
     /// <summary> 查询Http返回数据格式错误 </summary>
     ecHttpFormatError  = Ord(recrBase0) or $1000 or 2,
     /// <summary> 上传失败 </summary>
     ecHttpUploadFail   = Ord(recrBase0) or $1000 or 3,
     /// <summary> 下载失败 </summary>
     ecHttpDownloadFail = Ord(recrBase0) or $1000 or 4,
     /// <summary> Http Url 没有找到（文件不存在） </summary>
     ecHttpNotFound     = Ord(recrBase0) or $1000 or 5
  );
implementation

end.
